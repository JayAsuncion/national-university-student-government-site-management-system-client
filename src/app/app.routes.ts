import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthGuardService} from './auth/services/auth-guard.service';
import {LoginGuardService} from './auth/services/login-guard.service';

const routes: Routes = [
    {
        path: '',
        canActivate: [AuthGuardService],
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        data: {
            pageTitle: 'Dashboard',
            pageRoute: ''
        }
    },
    {
        path: 'home',
        canActivate: [AuthGuardService],
        loadChildren: './home/home.module#HomeModule',
        data: {
            pageTitle: 'Home Management',
            pageRoute: '/home',
            moduleCode: 'HOME'
        }
    },
    {
        path: 'pages',
        canActivate: [AuthGuardService],
        loadChildren: './pages/pages.module#PagesModule',
        data: {
            pageTitle: 'Pages Management',
            pageRoute: '/pages',
            moduleCode: 'EVENTS'
        }
    },
    {
        path: 'event-category',
        canActivate: [AuthGuardService],
        loadChildren: './event-category/event-category.module#EventCategoryModule',
        data: {
            pageTitle: 'Event Category',
            pageRoute: '/event-category',
            moduleCode: 'EVENTS'
        },
    },
    {
        path: 'auth',
        canActivate: [LoginGuardService],
        loadChildren: './auth/auth.module#AuthModule',
        data: {
            pageTitle: 'Auth',
            pageRoute: '/auth'
        }
    },
    {
        path: 'about-us',
        canActivate: [AuthGuardService],
        loadChildren: './about-us/about-us.module#AboutUsModule',
        data: {
            pageTitle: 'About Us',
            pageRoute: '/about-us',
            moduleCode: 'ABOUT_US'
        }
    },
    {
        path: 'contact-us',
        canActivate: [AuthGuardService],
        loadChildren: './contact-us/contact-us.module#ContactUsModule',
        data: {
            pageTitle: 'Contact Us',
            pageRoute: '/contact-us',
            moduleCode: 'CONTACT_US'
        },
    },
    {
        path: 'resolutions',
        canActivate: [AuthGuardService],
        loadChildren: './resolutions/resolutions.module#ResolutionsModule',
        data: {
            pageTitle: 'Resolutions',
            pageRoute: '/resolutions',
            moduleCode: 'RESOLUTIONS'
        },
    },
    {
        path: 'colleges',
        canActivate: [AuthGuardService],
        loadChildren: './colleges/colleges.module#CollegesModule',
        data: {
            pageTitle: 'Colleges',
            pageRoute: '/colleges',
            moduleCode: 'COLLEGES'
        },
    },
    {
        path: 'users',
        canActivate: [AuthGuardService],
        loadChildren: './users/users.module#UsersModule',
        data: {
            pageTitle: 'Users',
            pageRoute: '/users',
            moduleCode: 'USERS'
        },
    }

    // {
    //     path: '',
    //     // canActivate: [ AuthGuard ],
    //     component: DashboardComponent,
    //     data: {
    //         require_login: true,
    //         pageTitle: 'Dashboard',
    //         meta: {
    //             title: 'SMS 3 - Dashboard'
    //         }
    //     }
    // },
    // {
    //     path: 'login',
    //     canActivate: [ LoginGuard ],
    //     component: LoginComponent,
    //     data: {
    //         showNav: false
    //     }
    // }
];

@NgModule({
    imports: [RouterModule.forRoot(routes,  {onSameUrlNavigation: 'reload'})],
    exports: [RouterModule]
})
export class AppRoutes { }

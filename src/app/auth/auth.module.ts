import {NgModule} from '@angular/core';
import {LoginComponent} from './components/login/login.component';
import {AuthRoutes} from './auth.routes';
import {AuthService} from './services/auth.service';
import {LoginService} from './services/login.service';
import {AuthGuardService} from './services/auth-guard.service';
import {ReactiveFormsModule} from '@angular/forms';
import {LoginGuardService} from './services/login-guard.service';

@NgModule({
    imports: [
        AuthRoutes,
        ReactiveFormsModule
    ],
    declarations: [
        LoginComponent
    ],
    providers: [
        AuthService,
        AuthGuardService,
        LoginGuardService,
        LoginService
    ],
    exports: [

    ]
})
export class AuthModule {}

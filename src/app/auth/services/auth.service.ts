import {Injectable} from '@angular/core';
import {of, BehaviorSubject, Subject, Observable} from 'rxjs';
import * as moment from 'moment';
const TOKEN = 'TOKEN';

@Injectable({
    providedIn : 'root'
})

export class AuthService {
    public isLoggedInSource = new BehaviorSubject(false);
    public isUserLoggedIn = this.isLoggedInSource.asObservable();

    constructor() {
        this.isLoggedInSource.next(this.isLoggedIn());
    }

    public setAccessToken(token) {
        localStorage.setItem('ACCESS_TOKEN', token);
        this.isLoggedInSource.next(true);
        return of(true);
    }

    public getAccessToken() {
        return localStorage.getItem('ACCESS_TOKEN');
    }

    public isLoggedIn() {
        const accessToken = this.getAccessToken();

        if (!accessToken) {
            return false;
        }

        const data = this.parseAccessTokenData();

        if (!data) {
            return false;
        }

        return true;
    }

    public deleteAccessToken() {
        localStorage.removeItem('ACCESS_TOKEN');
        this.isLoggedInSource.next(false);
        return of(true);
    }

    private isAccessTokenExpired(exp) {
        const currentTimestamp = moment().unix();
        return currentTimestamp >= +exp;
    }

    public parseAccessTokenData() {
        const accessToken = this.getAccessToken();
        if (!accessToken) {
            return false;
        }

        const accessTokenData = accessToken.split('.');

        if (!accessTokenData[1]) {
            return false;
        } else {
            const tokenDecoded = JSON.parse(window.atob(accessTokenData[1]));

            if (!tokenDecoded.data.user.user_credential_id) {
                return false;
            }

            if (this.isAccessTokenExpired(tokenDecoded.exp)) {
                this.deleteAccessToken();
                return false;
            }

            return tokenDecoded;
        }
    }

    public getUserData() {
        const tokenData = this.parseAccessTokenData();
        return (tokenData !== false) ? tokenData.data.user : false;
    }

    public getUserCollegeID() {
        const tokenData = this.parseAccessTokenData();
        return (tokenData !== false) ? tokenData.data.user.college_id : undefined;
    }

    public getUserModules(): string[] {
        const tokenData = this.parseAccessTokenData();
        return (tokenData !== false) ? tokenData.data.user.user_type_modules : undefined;
    }
}

import { Injectable } from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { of, Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()

export class AuthGuardService implements CanActivate {

    constructor(
        private authService: AuthService,
        private router: Router,
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const fromRoute = state.url.substring(1);

        if (this.authService.isLoggedIn()) {
            const routeModuleCode = route.data.moduleCode;
            const userModules = this.authService.getUserModules();

            if (routeModuleCode === undefined) {
                // This means no need module code access, such as Dashboard
                return true;
            } else {
                if (userModules !== undefined && userModules.indexOf(routeModuleCode) !== -1) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            if (fromRoute !== undefined && fromRoute !== null && fromRoute.length > 0) {
                this.router.navigate(
                    ['/auth/login'],
                    {
                        queryParams : {
                            redirect :  fromRoute
                        }
                    }
                );
            } else {
                this.router.navigate(
                    ['/auth/login']
                );
            }

            return false;
        }
    }
}

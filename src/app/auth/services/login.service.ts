import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {Observable, Subject} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {catchError, takeUntil} from "rxjs/operators";
import {throwError} from 'rxjs';
import {AuthService} from "./auth.service";

@Injectable({
    providedIn: 'root'
})
export class LoginService {
    private apiUrl = environment.app.api_url;

    constructor(
        private httpClient: HttpClient
    ) {

    }

    loginHandler(loginFormData): Observable<any> {
        const url = this.apiUrl + '/auth';
        return this.httpClient.post(url, loginFormData, {observe: 'response'})
            .pipe(
                catchError((loginError: any) => throwError(loginError))
            );
    }
}

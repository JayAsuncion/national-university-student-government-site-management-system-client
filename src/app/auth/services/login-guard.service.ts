import { Injectable } from '@angular/core';
import {CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()

export class LoginGuardService implements CanActivate {

    constructor(
        private authService: AuthService,
        private router: Router,
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const fromRoute = state.url.substring(1);

        if (this.authService.isLoggedIn()) {
            this.router.navigate(['/']);
            return false;
        } else {
            return true;
        }
    }
}

import {Component, OnDestroy, OnInit} from '@angular/core';
import {LoginService} from '../../services/login.service';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FormTemplateSchemaService} from '../../../form-templates/services/form-template-schema.service';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['../../../../assets/css_min/modules/auth/login/login.min.css']
})
export class LoginComponent implements OnInit, OnDestroy {
    private destroyed$: Subject<{}> = new Subject();
    public loginForm: FormGroup;

    private isLoginFormSubmitted = false;

    constructor(
        private authService: AuthService,
        private activatedRoute: ActivatedRoute,
        private formTemplateDetailsService: FormTemplateSchemaService,
        private loginService: LoginService,
        private router: Router
    ) {

    }

    ngOnInit(): void {
        this.buildLoginForm();
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    private buildLoginForm() {
        this.loginForm = new FormGroup({
            email: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required])
        });
    }

    submitLoginForm() {
        this.isLoginFormSubmitted = true;

        if (this.loginForm.invalid) {
            Object.keys(this.loginForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.loginForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            return;
        }

        this.loginService.loginHandler(this.loginForm.value)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    const jwtToken = data.body.data.jwt_token;
                    this.authService.setAccessToken(jwtToken);

                    if (typeof this.activatedRoute.snapshot.queryParams.redirect !== 'undefined') {
                        this.router.navigate([`/${this.activatedRoute.snapshot.queryParams.redirect}`]);
                    } else {
                        this.router.navigate(['/']);
                    }

                },
                error => {
                    if (error.status === 401) {
                        alert(error.error.detail);
                    }
                }
            );
    }
}

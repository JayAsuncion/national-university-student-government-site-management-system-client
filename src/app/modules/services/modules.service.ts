import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ModulesService {
    private resourceUrl = environment.app.api_url + '/modules';

    constructor(
        private httpClient: HttpClient
    ) {}

    public getModuleList(): Observable<any> {
        return this.httpClient.get(this.resourceUrl);
    }
}

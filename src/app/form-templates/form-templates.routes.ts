import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {FormTemplateListComponent} from './components/form-template-list/form-template-list.component';

const routes: Routes = [
    {
        path: 'form-templates',
        component: FormTemplateListComponent,
        data: {
            pageTitle: 'Form Template List',
            pageRoute: 'form-templates',
            showHeader: true,
            showNav: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class FormTemplatesRoutes {

}

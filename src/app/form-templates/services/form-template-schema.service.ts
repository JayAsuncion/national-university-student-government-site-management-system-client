import {Injectable, OnInit} from '@angular/core';
import {Observable, Subject, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {catchError, takeUntil} from 'rxjs/operators';

@Injectable()

export class FormTemplateSchemaService implements OnInit {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url;
    private data: Subject<{}> = new Subject();

    constructor(
        private httpClient: HttpClient
    ) {}

    ngOnInit(): void {

    }

    fetchFormTemplate(templateCode) {
        const url = this.resourceUrl + '/form-templates/' + templateCode;
        this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
            data => {
                this.data.next(data);
            },
            error => {
                console.log(error);
            }
        );
    }

    getFormTemplate(templateCode): Observable<any> {
        this.fetchFormTemplate(templateCode);
        return this.data.asObservable();
    }

    fetchAllFormTemplateSchemaCodeAndName(): Observable<any> {
        const url = this.resourceUrl + '/form-templates' + '?column=template_name';
        return this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }
}

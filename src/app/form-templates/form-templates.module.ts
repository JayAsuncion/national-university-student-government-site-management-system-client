import {NgModule} from '@angular/core';
import {FormTemplateListComponent} from './components/form-template-list/form-template-list.component';
import {FormTemplateSchemaService} from './services/form-template-schema.service';

@NgModule({
    imports: [

    ],
    declarations: [
        FormTemplateListComponent
    ],
    providers: [
        FormTemplateSchemaService // TODO: Transfer service to Core Module if applicable
    ],
    exports: [

    ]
})
export class FormTemplatesModule { }

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Subject} from "rxjs";
import {takeUntil} from "rxjs/operators";

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    public pageTitle: any;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
        this.router.events.pipe(takeUntil(this.destroyed$))
            .subscribe(e => {
            if (e instanceof NavigationEnd) {
                this.pageTitle = this.router.routerState.root.data;
            }
        });
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }
}

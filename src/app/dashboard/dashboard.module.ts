import { NgModule } from '@angular/core';
import { DashboardComponent } from './components/dashboard.component';
import { DashboardRoutes } from './dashboard.routes';

@NgModule({
    imports: [
        DashboardRoutes
    ],
    declarations: [
        DashboardComponent
    ],
    providers: [

    ],
    exports: [
    ]
})
export class DashboardModule {}

import { NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard.component';

const routes: Routes = [
    {
        path: '',
        component : DashboardComponent,
        data: {
            pageTitle: 'Dashboard',
            pageRoute: '/',
            showHeader: true,
            showNav: true
        }
    }
];
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class DashboardRoutes {}

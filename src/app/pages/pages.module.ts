import { NgModule } from '@angular/core';
import { PageListComponent } from './components/page-list/page-list.component';
import { PagesRoutes } from './pages.routes';
import { PageDetailsComponent } from './components/page-details/page-details.component';
import {CommonModule} from '@angular/common';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {PagesService} from './services/pages.service';
import {BsDatepickerModule} from 'ngx-bootstrap/datepicker';
import {TabsModule} from 'ngx-bootstrap/tabs';
import {ReactiveFormsModule} from '@angular/forms';
import {PageCategoriesService} from './services/page-categories.service';
import {SharedModule} from '../shared/shared.module';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {EventCategoryModule} from '../event-category/event-category.module';
import {PageTemplatesService} from './services/page-templates.service';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        PagesRoutes,
        NgxDatatableModule,
        TabsModule.forRoot(),
        BsDatepickerModule.forRoot(),
        ReactiveFormsModule,
        CKEditorModule,
        EventCategoryModule
    ],
    declarations: [
        PageListComponent,
        PageDetailsComponent
    ],
    providers: [
        PagesService,
        PageCategoriesService,
        PageTemplatesService
    ],
    exports: [
    ]
})
export class PagesModule {}

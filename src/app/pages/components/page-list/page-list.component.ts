import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ColumnMode, SelectionType} from '@swimlane/ngx-datatable';
import {PagesService} from '../../services/pages.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {environment} from "../../../../environments/environment.local";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from '../../../auth/services/auth.service';

@Component({
    selector: 'app-page-list',
    templateUrl: 'page-list.component.html',
    styleUrls: ['../../../../assets/css_min/modules/pages/page-list/page-list.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class PageListComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    private siteURL = environment.app.site_url;

    private appConfig;
    isPageListLoading = true;
    pageSelected = [];
    ColumnMode = ColumnMode;
    SelectionType = SelectionType;
    pageListData: any[];

    pageCategoryCode = '';
    collegeID = 1;
    pageTitle = '';

    constructor(
        private activatedRoute: ActivatedRoute,
        private authService: AuthService,
        private router: Router,
        private pagesService: PagesService
    ) {
    }

    ngOnInit(): void {
        this.collegeID = this.authService.getUserCollegeID();
        this.appConfig = environment.app;
        this.pageTitle = this.activatedRoute.snapshot.data.pageTitle;
        this.pageCategoryCode = this.activatedRoute.snapshot.data.pageCategoryCode;
        if (this.pageCategoryCode !== undefined && this.pageCategoryCode !== null) {
            this.pagesService.fetchPageList({page_category_code: this.pageCategoryCode, college_id: this.collegeID})
                .pipe(takeUntil(this.destroyed$))
                .subscribe(
                    response => {
                        this.pageListData = response.data.pageList;
                        this.isPageListLoading = false;
                    },
                    error => {
                        console.log(error);
                    }
                );
        } else {
            this.router.navigate(['/']);
        }
    }

    public previewPage() {
        let previewURL = this.siteURL;

        if (this.pageCategoryCode == 'EVENT_PAGE') {
            previewURL += '/pages/event_page';
        } else if (this.pageCategoryCode == 'CONGRESS_SESSION_PAGE') {
            previewURL += '/pages/congress_session_page';
        }

        window.open(previewURL, '_blank');
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    onPageListHover(event) {
        console.log('Activate');
    }

    onPageListSelect(pageList) {
        this.router.navigate(['/pages/' + pageList.selected[0].page_detail_id]);
    }
}

import {AfterViewInit, Component, EventEmitter, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {PagesService} from '../../services/pages.service';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {PageDetailModel} from '../../../shared/models/page-detail.model';
import {PageCategory} from '../../../shared/models/page-category.model';
import {PageCategoriesService} from '../../services/page-categories.service';
import * as DocumentEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import {ChangeEvent} from '@ckeditor/ckeditor5-angular';
import {EventCategoryService} from '../../../event-category/services/event-category.service';
import {EventCategory} from '../../../shared/models/event-category.model';
import {PageTemplatesService} from '../../services/page-templates.service';
import {PageTemplateModel} from '../../../shared/models/page-template.model';
import {environment} from '../../../../environments/environment';
import {CollegesService} from '../../../colleges/services/colleges.service';
import {AuthService} from '../../../auth/services/auth.service';

@Component({
    selector: 'app-page-details',
    templateUrl: 'page-details.component.html',
    styleUrls: ['../../../../assets/css_min/modules/pages/page-details/page-details.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class PageDetailsComponent implements OnInit, AfterViewInit, OnDestroy {
    private destroyed$ = new Subject();

    private siteURL = environment.app.site_url;

    // Page Data
    public pageIDFromRoute: number;
    private pageDetailID = 0;
    public pageDetailsData = new PageDetailModel();

    // Page Details
    public pageDetailsForm: FormGroup;
    private collegeID;
    public collegeName;
    public collegeList = [];
    public pageCategories: PageCategory[];
    public pageTemplateList: PageTemplateModel[];

    // Page Content
    public pageContentForm: FormGroup;
    public pageContentID = 0;
    private formTemplateContentID = 0;
    public formTemplateContentCode = '';
    public pageContentData = {
        event_name: '',
        event_date: '',
        event_category: '',
        event_images: '',
        event_content: '',
        sponsors_partners: ''
    };
    public enabledFormControls = {
        event_category: true,
        sponsors_partners: true
    };

    // private eventContentHolder;
    public eventCategories: EventCategory[];

    public isPageLoaded = false;
    public isPageSubmitted = false;

    // Tabs
    public tabs: any[] = [
        { title: 'Page Details', content: 'pageDetails', active: true},
        { title: 'Page Content', content: 'pageContent'}
    ];

    public eventContentEditor = DocumentEditor;
    public eventContentEditorConfig  = {
        simpleUpload: {
            uploadUrl: `${environment.app.image_uploader_url}/images?uploader=CK_EDITOR`
        }
    };

    constructor(
        private activatedRoute: ActivatedRoute,
        private authService: AuthService,
        private eventCategoryService: EventCategoryService,
        private collegesService: CollegesService,
        private pageTemplatesService: PageTemplatesService,
        private pagesService: PagesService,
        private pageCategoriesService: PageCategoriesService,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        this.pageIDFromRoute = this.activatedRoute.snapshot.params.page_detail_id;
        this.pageDetailID = this.pageIDFromRoute;
        this.collegeID = this.authService.getUserCollegeID();

        if (typeof this.pageIDFromRoute !== 'undefined' && isNaN(this.pageIDFromRoute)) {
            this.router.navigate(['/']);
        }

        // Create Page
        if (typeof this.pageIDFromRoute === 'undefined') {

        } else {
            this.loadPage(this.pageIDFromRoute);
        }

        // Page Details Tab
        this.buildPageDetailsForm();
        // Load Dropdown Values
        this.loadCollegeListForDropdown();
        this.loadPageCategoriesForDropdown();
        this.loadPageTemplateListForDropdown();

        // Page Content Tab
        this.buildPageContentForm();
        // Load Dropdown Values
        this.loadEventCategoriesForDropdown();
    }

    ngAfterViewInit(): void {

    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    private buildPageDetailsForm() {
        this.pageDetailsForm = new FormGroup({
            page_name: new FormControl('', [Validators.required]),
            page_url: new FormControl(''),
            page_description: new FormControl('', [Validators.required]),
            page_category_code: new FormControl('', [Validators.required]),
            template_code: new FormControl('', [Validators.required])
        });
    }

    private loadPage(pageID) {
        this.pagesService.fetchPage(pageID)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    if (!data.success) {
                        this.goBackToPageList();
                        return;
                    }

                    this.isPageLoaded = true;
                    this.pageDetailsData = data.data.page_details;
                    console.log(this.pageDetailsData);
                    this.pageDetailsForm.patchValue({
                        page_name : this.pageDetailsData.page_name,
                        page_url : this.pageDetailsData.page_url,
                        page_description: this.pageDetailsData.page_description,
                        page_category_code: this.pageDetailsData.page_category_code,
                        template_code : this.pageDetailsData.template_code
                    });
                    this.formTemplateContentID = data.data.page_content.form_template_content_id;
                    this.formTemplateContentCode = data.data.form_template_content_code;
                    this.pageContentData = data.data.form_template_content;
                    this.resetEnabledFormControls();
                    this.patchPageContentForm(this.pageContentData);
                },
                error => {
                    console.log(error);
                }
            );
    }

    loadCollegeListForDropdown() {
        this.collegesService.getCollegeList()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.collegeList = response.data.colleges;
                        this.setCollegeName(this.collegeList);
                    }
                },
                error => {

                }
        );
    }

    private setCollegeName(collegeList) {
        for (let i = 0; i < collegeList.length; i++) {
            if (this.collegeID === collegeList[i].college_id) {
                this.collegeName = collegeList[i].college_name;
            }
        }
    }

    loadPageCategoriesForDropdown() {
       this.pageCategoriesService.fetchPageCategoryList()
           .pipe(takeUntil(this.destroyed$))
           .subscribe(
               data => {
                   this.pageCategories = data.data.page_categories;
               },
               error => {
                   console.log(error);
               }
           );
    }

    loadPageTemplateListForDropdown() {
        this.pageTemplatesService.fetchPageTemplateList()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    this.pageTemplateList = data.data.page_template_list;
                },
                error => {
                    console.log(error);
                }
            );
    }

    private buildPageContentForm() {
        this.pageContentForm = new FormGroup({
            event_name: new FormControl('', [Validators.required]),
            event_date: new FormControl('', [Validators.required]),
            event_category: new FormControl(''),
            event_images: new FormControl(''),
            event_content: new FormControl('', [Validators.required]),
            sponsors_partners: new FormControl('')
        });
    }

    public patchPageContentForm(pageContentData) {
        this.pageContentForm.patchValue({event_name: pageContentData.event_name});
        this.pageContentForm.patchValue({event_date: pageContentData.event_date});

        if (this.isPageLoaded && this.enabledFormControls.event_category) {
            this.pageContentForm.patchValue({event_category: pageContentData.event_category});
        }

        this.pageContentForm.patchValue({event_images: pageContentData.event_images});
        this.pageContentForm.patchValue({event_content: pageContentData.event_content});

        if (this.isPageLoaded && this.enabledFormControls.sponsors_partners) {
            this.pageContentForm.patchValue({sponsors_partners: pageContentData.sponsors_partners});
        }
    }

    private resetEnabledFormControls() {
        if (this.formTemplateContentCode === 'CONGRESS_SESSION_FORM_TPL') {
            console.log('CONGRESS_SESSION_FORM_TPL');
            this.enabledFormControls = {
                event_category: false,
                sponsors_partners: false
            };
        }

        if (this.formTemplateContentCode === 'EVENT') {
            this.enabledFormControls = {
                event_category: true,
                sponsors_partners: true
            };
        }
    }

    public onEventContentEditorReady( editor ) {
        editor.ui.getEditableElement().parentElement.insertBefore(
            editor.ui.view.toolbar.element,
            editor.ui.getEditableElement()
        );      //
        editor.simpleUpload = {uploadUrl: 'http://sms-api.jintuitive.tech/images'};
    }

    public onMainContentChange( { editor }: ChangeEvent ) {
        // this.eventContentHolder = editor.getData();
    }

    public pageCreatorCreateEvent() {
        this.isPageSubmitted = true;
        let isPageDetailsFormValid = true;
        let isPageContentFormValid = true;

        if (this.pageDetailsForm.invalid) {
            Object.keys(this.pageDetailsForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.pageDetailsForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            isPageDetailsFormValid = false;
        }

        if (this.pageContentForm.invalid) {
            Object.keys(this.pageContentForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.pageContentForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            isPageContentFormValid = false;
        }

        if (!isPageDetailsFormValid || !isPageContentFormValid) {
            return;
        }

        const pageDetailsFormData = this.pageDetailsForm.value;
        pageDetailsFormData['college_id'] = this.collegeID;
        const pageContentFormData = this.pageContentForm.value;

        const requestPayload = {page_details: pageDetailsFormData, page_content: pageContentFormData};

        this.pagesService.createPage(requestPayload)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    this.pageDetailID = data.data.page_detail_id;
                    this.router.navigate(['/pages/' + this.pageDetailID]);
                },
                error => {
                    console.log('pageCreatorCreateEvent', error);
                }
            );
    }

    public pageEditorUpdateEvent() {
        this.isPageSubmitted = true;

        if (this.pageDetailsForm.invalid) {
            Object.keys(this.pageDetailsForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.pageDetailsForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            return;
        }

        if (this.pageContentForm.invalid) {
            Object.keys(this.pageContentForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.pageContentForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            return;
        }

        if (this.pageDetailsForm.invalid || this.pageContentForm.invalid) {
            return;
        }

        const pageDetailsFormData = this.pageDetailsForm.value;
        const pageContentFormData = this.pageContentForm.value;

        const requestPayload = {
            page_details: pageDetailsFormData,
            page_content: {form_template_content_id: this.formTemplateContentID},
            form_template_content: pageContentFormData
        };

        this.pagesService.updatePage(this.pageDetailID, requestPayload)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    if (data.success) {
                        this.router.navigate([`/pages/${data.data.page_detail_id}`]);
                    }
                },
                error => {
                    console.log('pageEditorUpdateEvent', error);
                }
            );
    }

    public pageEditorDeleteEvent() {
        this.pagesService.deletePage(this.pageDetailID)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
            response => {
                    if (response.success) {
                        this.goBackToPageList();
                    }
                },
                error => {
                    console.log('pageEditorDeleteEvent', error);
                }
            )
    }

    public previewPage() {
        let previewURL = this.siteURL;

        if (this.formTemplateContentCode === 'CONGRESS_SESSION_FORM_TPL') {
            previewURL += '/pages/congress_session_page';
        } else if (this.formTemplateContentCode === 'EVENT') {
            previewURL += '/pages/event_page'
        }

        previewURL += '/' + this.pageDetailID;
        window.open(previewURL, '_blank');
    }

    public goBackToPageList() {
        if (this.formTemplateContentCode === 'CONGRESS_SESSION_FORM_TPL') {
            this.router.navigate(['/pages/category/CONGRESS_SESSION_PAGE']);
        } else if (this.formTemplateContentCode === 'EVENT') {
            this.router.navigate(['/pages/category/EVENT_PAGE']);
        }
    }

    public resetForms() {
        this.pageDetailsForm.reset();
        this.pageContentForm.patchValue({
            event_name: '',
            event_date: '',
            event_category: '',
            event_images: '',
            event_content: '',
            sponsors_partners: ''
        });
    }

    public loadEventCategoriesForDropdown() {
        this.eventCategoryService.fetchEventCategoryList()
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    this.eventCategories = data.data.event_category_list;
                },
                error => {
                    console.log('loadEventCategoriesForDropdown', error);
                }
            );
    }

    public pageCategoryChange(event) {
        let pageCategoryValue = event.target.value;

        if (pageCategoryValue === 'EVENT_PAGE') {
            this.formTemplateContentCode = 'EVENT';
        } else if (pageCategoryValue === 'CONGRESS_SESSION_PAGE') {
            this.formTemplateContentCode = 'CONGRESS_SESSION_FORM_TPL';
        }

        this.resetEnabledFormControls();
    }

    get pC() {
        return this.pageContentForm.controls;
    }
    test() {
        console.log(this.pageContentForm.controls['event_images'].value);
    }
}

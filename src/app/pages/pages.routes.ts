import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageListComponent} from './components/page-list/page-list.component';
import {PageDetailsComponent} from './components/page-details/page-details.component';
import {AuthGuardService} from '../auth/services/auth-guard.service';

const routes: Routes = [
    {
        path: '',
        component: PageListComponent,
        canActivate: [AuthGuardService],
        data: {
            pageTitle: 'Pages List',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: 'category/EVENT_PAGE',
        component: PageListComponent,
        canActivate: [AuthGuardService],
        data: {
            pageTitle: 'Event List',
            pageRoute: '',
            showNav: true,
            showHeader: true,
            moduleCode: 'EVENTS',
            pageCategoryCode: 'EVENT_PAGE'
        }
    },
    {
        path: 'category/CONGRESS_SESSION_PAGE',
        component: PageListComponent,
        canActivate: [AuthGuardService],
        data: {
            pageTitle: 'Congress Session List',
            pageRoute: '',
            showNav: true,
            showHeader: true,
            moduleCode: 'CONGRESS_SESSIONS',
            pageCategoryCode: 'CONGRESS_SESSION_PAGE'
        }
    },
    {
        path: 'create',
        component: PageDetailsComponent,
        data: {
            pageTitle: 'Create Page',
            pageRoute: 'create',
            showHeader: true,
            showNav: true
        }
    },
    {
        path: ':page_detail_id',
        component: PageDetailsComponent,
        data: {
            pageTitle: 'Pages Details',
            pageRoute: 'page-details',
            showHeader: true,
            showNav: true
        }
    }
];
@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class PagesRoutes {}

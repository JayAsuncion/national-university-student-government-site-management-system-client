import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {catchError, takeUntil} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class PagesService implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url;
    private pageListData = new Subject();

    constructor(
        private httpClient: HttpClient
    ) {}

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    createPage(pageData): Observable<any> {
        const url = this.resourceUrl + '/pages';
        return this.httpClient.post(url, pageData)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }

    updatePage(pageDetailID, pageData): Observable<any> {
        const url = this.resourceUrl + '/pages';
        return this.httpClient.put(`${url}/${pageDetailID}`, pageData)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }

    deletePage(pageDetailID): Observable<any> {
        const url = this.resourceUrl + '/pages/' + pageDetailID;
        return this.httpClient.put(url, {delete_flag: 'y'});
    }

    fetchPageListxxx() {
        const url = this.resourceUrl + '/pages';
        return this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    this.pageListData.next(data);
                },
                error => {
                    console.log(error);
                }
            );
    }

    fetchPageList(params): Observable<any> {
        const url = this.resourceUrl + `/pages?page_category_code=${params.page_category_code}&college_id=${params.college_id}`;
        return this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }

    getPageList(): Observable<any> {
        this.fetchPageList('EVENT_PAGE');
        return this.pageListData.asObservable();
    }

    fetchPage(pageID): Observable<any> {
        const url = this.resourceUrl + '/pages/' + pageID;
        return this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }
}

import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {catchError, takeUntil} from 'rxjs/operators';

@Injectable()

export class PageTemplatesService implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url;
    private pageListData = new Subject();

    constructor(
        private httpClient: HttpClient
    ) {}

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    createPage(pageData): Observable<any> {
        const url = this.resourceUrl + '/pages';
        return this.httpClient.post(url, pageData)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }

    fetchPageListxxx() {
        const url = this.resourceUrl + '/pages';
        return this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    this.pageListData.next(data);
                },
                error => {
                    console.log(error);
                }
            );
    }

    fetchPageTemplateList(): Observable<any> {
        const url = this.resourceUrl + '/page-templates?sms_flag=y';
        return this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }

    getPageList(): Observable<any> {
        this.fetchPageTemplateList();
        return this.pageListData.asObservable();
    }

    fetchPage(pageID): Observable<any> {
        const url = this.resourceUrl + '/pages/' + pageID;
        return this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }
}

import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {catchError, takeUntil} from 'rxjs/operators';

@Injectable()

export class PageCategoriesService implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url;

    constructor(
        private httpClient: HttpClient
    ) {}

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    fetchPageCategoryList(): Observable<any> {
        const url = this.resourceUrl + '/page-categories?sms_flag=y';
        return this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }
}

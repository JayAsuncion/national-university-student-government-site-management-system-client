import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from "rxjs";
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";
import {takeUntil} from "rxjs/operators";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit, OnDestroy {
    public showHeader = true;
    public showNav = true;

    private destroyed$: Subject<{}> = new Subject();
    public items: Array<any> = [];

    public isLeftNavCollapsed: boolean;

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router
    ) {

    }

    ngOnInit(): void {
        this.router.events.pipe(takeUntil(this.destroyed$)).subscribe( e => {
            if (e instanceof NavigationEnd) {
                this.items = [];
                this.extract(this.router.routerState.root);
            }
        });
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    extract(route) {
        const pageTitle = route.data.value.pageTitle;
        const pageRoute = route.data.value.pageRoute;

        if (pageTitle && pageRoute && this.items.indexOf(pageTitle) == -1) {
            if (typeof route.data.value.pageTitle === 'object') {
                for (let i = 0; i < route.data.value.pageTitle.length; i++) {
                    this.items.push({
                        pageTitle: route.data.value.pageTitle[i],
                        pageRoute: route.data.value.pageRoute[i]
                    });
                    if (route.data.value.showNav != undefined && route.data.value.showHeader != undefined) {
                        this.showHeader = route.data.value.showHeader;
                        this.showNav = route.data.value.showNav;
                    }
                }
            } else {
                this.items.push({
                    pageTitle: route.data.value.pageTitle,
                    pageRoute: route.data.value.pageRoute
                });
                const routeData = this.activatedRoute.snapshot.firstChild.firstChild.data;

                if (routeData.showHeader !== undefined  && routeData.showNav !== undefined) {
                    this.showHeader = this.activatedRoute.snapshot.firstChild.firstChild.data.showHeader ;
                    this.showNav = this.activatedRoute.snapshot.firstChild.firstChild.data.showNav;
                }
            }
        }

        if (route.children) {
            route.children.forEach(it => {
                this.extract(it);
            });
        }
    }

    public toggleLeftNav(isLeftNavCollapsed) {
        this.isLeftNavCollapsed = isLeftNavCollapsed;
    }
}

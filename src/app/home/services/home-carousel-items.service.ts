import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class HomeCarouselItemsService {
    private resourceUrl = environment.app.api_url + '/home-carousel-items';

    constructor(
        protected httpClient: HttpClient
    ) {}

    public loadHomeCarouselItems(): Observable<any> {
        return this.httpClient.get(this.resourceUrl);
    }

    public insertHomeCarouselItem(formData): Observable<any> {
        return this.httpClient.post(this.resourceUrl, formData);
    }

    public updateHomeCarouselItem(carouselItemID, formData): Observable<any> {
        const url = this.resourceUrl + `/${carouselItemID}`;
        return this.httpClient.put(url, formData);
    }

    public deleteHomeCarouselItem(carouselItemID): Observable<any> {
        const url = this.resourceUrl + `/${carouselItemID}`;
        const formData = {delete_flag: 'y'};
        return this.httpClient.put(url, formData);
    }
}

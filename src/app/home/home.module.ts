import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {HomeCarouselComponent} from './components/home-carousel/home-carousel.component';
import {HomeRoutes} from './home.routes';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        HomeRoutes,
        ReactiveFormsModule
    ],
    declarations: [
        HomeCarouselComponent
    ],
    providers: [
    ],
    exports: [
    ]
})
export class HomeModule {}

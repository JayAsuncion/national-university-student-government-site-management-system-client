import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {HomeCarouselItemsService} from '../../services/home-carousel-items.service';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-home-carousel',
    templateUrl: './home-carousel.component.html',
    styleUrls: ['../../../../assets/less/modules/home/home-carousel/home-carousel.less'],
})
export class HomeCarouselComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    private siteURL = environment.app.site_url;

    public carouselItemForm: FormGroup;
    public showCarouselItemFormFlag = false;
    public showAddFormButton = false;
    public showSaveFormButton = false;
    public showCancelFormButton = false;
    public showDeleteFormButton = false;
    public showPreviewPageButton = true;
    public isCarouselItemFormSubmitted = false;
    public selectedCarouselItemIndex = -1;
    public formTitle = '';

    public pageStates = {
        listMode: {
            formTitle: '',
            showCarouselItemFormFlag: false,
            showAddFormButton: false,
            showSaveFormButton: false,
            showCancelFormButton: false,
            showDeleteFormButton: false,
            showPreviewPageButton: true
        },
        addMode: {
            formTitle: 'Add Banner Item',
            showCarouselItemFormFlag: true,
            showAddFormButton: true,
            showSaveFormButton: false,
            showCancelFormButton: true,
            showDeleteFormButton: false,
            showPreviewPageButton: false
        },
        editMode: {
            formTitle: 'Edit Banner Item',
            showCarouselItemFormFlag: true,
            showAddFormButton: false,
            showSaveFormButton: true,
            showCancelFormButton: true,
            showDeleteFormButton: true,
            showPreviewPageButton: false
        }
    };

    public carouselItems = [];

    public carouselItemFormData = {
        label: '',
        url: []
    };

    constructor(
        protected homeCarouselItemService: HomeCarouselItemsService
    ) {}

    ngOnInit() {
        this.loadCarouselItems();
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    private loadCarouselItems() {
        this.homeCarouselItemService.loadHomeCarouselItems()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.parseCarouselItemsData(response.data.home_carousel_item_list);
                    }
                },
                error => {

                }
        );
    }

    public parseCarouselItemsData(carouselItemsData) {
        carouselItemsData.forEach((value) => {
            value.url = JSON.parse(value.url);
        });

        this.carouselItems = carouselItemsData;
    }

    public showCarouselItemAddForm() {
        this.createCarouselItemForm();
        this.setPageStates('addMode');
        this.resetForms();

        this.selectedCarouselItemIndex = -1;
    }

    public resetForms() {
        this.carouselItemForm.patchValue({
            label: '',
            url: []
        });
        this.carouselItemFormData = {
            label: '',
            url: []
        };
    }

    public createCarouselItem() {
        if (this.isCarouselItemFormSubmitted) {
            return;
        }

        if (!this.carouselItemForm.valid) {
            Object.keys(this.carouselItemForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.carouselItemForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            return;
        }

        this.isCarouselItemFormSubmitted = true;
        const carouselItemFormData = this.carouselItemForm.value;
        this.homeCarouselItemService.insertHomeCarouselItem(carouselItemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                if (response.success) {
                    carouselItemFormData['carousel_item_id'] = response.data.carousel_item_id;
                    this.carouselItems.push(carouselItemFormData);
                    this.setPageStates('listMode');
                }

                this.isCarouselItemFormSubmitted = false;
            },
            error => {

            }
        );
    }

    public loadCarouselItem(carouselItemIndex) {
        this.createCarouselItemForm();
        this.selectedCarouselItemIndex = carouselItemIndex;
        this.setPageStates('editMode');
        this.carouselItemFormData = this.carouselItems[carouselItemIndex];
        this.patchCarouselItemForm(this.carouselItems[carouselItemIndex]);
    }

    protected createCarouselItemForm() {
        if (typeof this.carouselItemForm === 'undefined') {
            this.carouselItemForm = new FormGroup({
                label: new FormControl(''),
                url: new FormControl('', [Validators.required])
            });
        }
    }

    public setPageStates(mode: string) {
        this.formTitle = this.pageStates[mode].formTitle;
        this.showCarouselItemFormFlag = this.pageStates[mode].showCarouselItemFormFlag;
        this.showAddFormButton = this.pageStates[mode].showAddFormButton;
        this.showSaveFormButton = this.pageStates[mode].showSaveFormButton;
        this.showCancelFormButton = this.pageStates[mode].showCancelFormButton;
        this.showDeleteFormButton = this.pageStates[mode].showDeleteFormButton;
        this.showPreviewPageButton = this.pageStates[mode].showPreviewPageButton;
    }

    patchCarouselItemForm(carouselItemData) {
        this.carouselItemForm.patchValue({
            label: carouselItemData.label,
            url: carouselItemData.url
        });
    }

    saveCarouselItem() {
        if (this.isCarouselItemFormSubmitted) {
            return;
        }

        if (!this.carouselItemForm.valid) {
            Object.keys(this.carouselItemForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.carouselItemForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            return;
        }

        this.isCarouselItemFormSubmitted = true;
        const carouselItemFormData = this.carouselItemForm.value;
        const carouselItemID = this.carouselItems[this.selectedCarouselItemIndex].carousel_item_id;
        this.homeCarouselItemService.updateHomeCarouselItem(carouselItemID, carouselItemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        const updatedCarouselItemData = carouselItemFormData;
                        updatedCarouselItemData['carousel_item_id'] = carouselItemID;
                        this.carouselItems[this.selectedCarouselItemIndex] = carouselItemFormData;
                        this.setPageStates('listMode');
                    }

                    this.isCarouselItemFormSubmitted = false;
                },
                error => {

                }
            );
    }

    public deleteCarouselItem() {
        if (this.isCarouselItemFormSubmitted) {
            return;
        }

        this.isCarouselItemFormSubmitted = true;
        const carouselItemID = this.carouselItems[this.selectedCarouselItemIndex].carousel_item_id;

        this.homeCarouselItemService.deleteHomeCarouselItem(carouselItemID)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.carouselItems.splice(this.selectedCarouselItemIndex, 1);
                        this.setPageStates('listMode');
                    }
                },
                error => {

                }
        );
    }

    public previewPage() {
        const previewURL = this.siteURL + '/about-us';
        window.open(previewURL, '_blank');
    }

    get fC() {
        return this.carouselItemForm.controls;
    }
}

import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeCarouselComponent} from './components/home-carousel/home-carousel.component';

const routes: Routes = [
    {
        path: '',
        component: HomeCarouselComponent,
        data: {
            pageTitle: 'Manage Home Carousel',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class HomeRoutes {}

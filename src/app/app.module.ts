import { NgModule } from '@angular/core';

import { SharedModule } from './shared/shared.module';
import { AppRoutes } from './app.routes';
import { AppComponent } from './app.component';
import {DashboardModule} from './dashboard/dashboard.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormTemplatesModule} from './form-templates/form-templates.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ImagesModule} from './images/images.module';
import {AuthHttpInterceptor} from './auth/interceptor/auth-http.interceptor';
import {AuthModule} from './auth/auth.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserAnimationsModule,
        SharedModule,
        HttpClientModule,
        AppRoutes,
        DashboardModule,
        FormTemplatesModule,
        NgxDatatableModule,
        BrowserAnimationsModule,
        ImagesModule,
        AuthModule
    ],
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true}
    ],
    bootstrap: [AppComponent]
    })
export class AppModule {}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CollegesService {
    private resourceUrl = environment.app.api_url;

    constructor(
        private httpClient: HttpClient
    ) {}

    public getCollegeList(isCollege = 'y'): Observable<any> {
        const url = this.resourceUrl + '/colleges';
        return this.httpClient.get(url);
    }

    public getCollege(collegeID): Observable<any> {
        const url = this.resourceUrl + `/colleges/${collegeID}`;
        return this.httpClient.get(url);
    }

    public insertCollege(formData): Observable<any> {
        const url = this.resourceUrl + '/colleges';
        return this.httpClient.post(url, formData);
    }

    public updateCollege(collegeID, formData): Observable<any> {
        const url = this.resourceUrl + '/colleges/' + collegeID;
        return this.httpClient.put(url, formData);
    }

    public deleteCollege(collegeID): Observable<any> {
        const url = this.resourceUrl + '/colleges/' + collegeID;
        const formData = {delete_flag: 'y'};
        return this.httpClient.put(url, formData);
    }
}

import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CollegesListComponent} from './components/colleges-list.component';
import {CollegeItemComponent} from './components/college-item/college-item.component';

const routes: Routes = [
    {
        path: '',
        component: CollegesListComponent,
        data: {
            pageTitle: 'Colleges',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: 'create',
        component: CollegeItemComponent,
        data: {
            pageTitle: 'Add College',
            pageRoute: 'create',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: ':college_id',
        component: CollegeItemComponent,
        data: {
            pageTitle: 'CollegePage',
            pageRoute: 'college-item',
            showNav: true,
            showHeader: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class CollegesRoutes {}

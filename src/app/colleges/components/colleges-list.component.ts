import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {ColumnMode, SelectionType} from '@swimlane/ngx-datatable';
import {Router} from '@angular/router';
import {CollegesService} from '../services/colleges.service';
import {takeUntil} from 'rxjs/operators';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-colleges-list',
    templateUrl: './colleges-list.component.html',
    styleUrls: ['../../../assets/css_min/modules/colleges/colleges-list/colleges-list.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class CollegesListComponent implements OnInit {
    private destroyed$ = new Subject();

    ColumnMode = ColumnMode;
    SelectionType = SelectionType;

    isCollegeListLoading = true;
    collegeListData: any[];
    collegeSelected = [];

    constructor(
        private collegesService: CollegesService,
        private router: Router
    ) {}

    static processCollegeListData(collegesData) {
        let processedCollegesData = [];

        for (const college of collegesData) {
            let collegeData = college;

            if (collegeData.college_logo.length == 0) {
                continue;
            }

            let collegeLogo = JSON.parse(collegeData.college_logo);
            collegeData['college_logo_url'] = collegeLogo[0].url;
            processedCollegesData.push(collegeData);
        }

        return processedCollegesData;
    }

    ngOnInit(): void {
        this.loadCollegeList();
    }

    private loadCollegeList() {
        this.collegesService.getCollegeList()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.collegeListData = CollegesListComponent.processCollegeListData(response.data.colleges);
                    }

                    this.isCollegeListLoading = false;
                },
            error => {
                console.log('loadCollegeList', error);
            }
        );
    }

    public previewPage() {
        const previewURL = environment.app.site_url + '/colleges';
        window.open(previewURL, '_blank');
    }

    onCollegeListHover(event) {
        console.log('Activate');
    }

    onCollegeListSelect(collegeList) {
        this.router.navigate(['/colleges/' + collegeList.selected[0].college_id]);
    }
}

import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {CollegesService} from '../../services/colleges.service';
import * as DocumentEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import {environment} from '../../../../environments/environment';
import {ChangeEvent} from '@ckeditor/ckeditor5-angular';

@Component({
    selector: 'app-college-item',
    templateUrl: 'college-item.component.html',
    styleUrls: ['../../../../assets/css_min/modules/colleges/college-item/college-item.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class CollegeItemComponent implements OnInit {
    private destroyed$ = new Subject();
    private appConfig = environment.app;

    public collegeIDFromRoute;

    public collegeItemForm: FormGroup;
    public collegeItemData = {
        college_name: '',
        college_description: '',
        college_logo: '',
        college_courses_offered: '',
        college_facebook_url: ''
    };
    public isCollegeItemFormSubmitted = false;
    public isCollegeItemLoaded = false;

    public collegeDescriptionEditor = DocumentEditor;
    public collegeDescriptionEditorConfig  = {
        simpleUpload: {
            uploadUrl: `${environment.app.image_uploader_url}/images?uploader=CK_EDITOR`
        }
    };

    constructor(
        private activatedRoute: ActivatedRoute,
        private collegesService: CollegesService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.buildCollegeItemForm();

        this.collegeIDFromRoute = this.activatedRoute.snapshot.params.college_id;

        // Create Form
        if (this.collegeIDFromRoute === undefined) {

        } else {
            this.loadCollegeItem(this.collegeIDFromRoute);
        }
    }

    private loadCollegeItem(collegeID) {
        this.collegesService.getCollege(collegeID)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.collegeItemData = response.data.college;
                        this.collegeItemData.college_logo = JSON.parse(this.collegeItemData.college_logo);
                        this.collegeItemData.college_courses_offered = JSON.parse(this.collegeItemData.college_courses_offered);
                        this.collegeItemForm.patchValue({
                            college_name: this.collegeItemData.college_name,
                            college_description: this.collegeItemData.college_description,
                            college_logo: this.collegeItemData.college_logo,
                            college_courses_offered: this.collegeItemData.college_courses_offered,
                            college_facebook_url: this.collegeItemData.college_facebook_url
                        });
                        this.isCollegeItemLoaded = true;
                    } else {
                        this.router.navigate(['/colleges']);
                    }
                },
            error => {
                console.log('loadResolutionItem', error);
            }
        );
    }

    private buildCollegeItemForm() {
        this.collegeItemForm = new FormGroup({
            college_name: new FormControl('', [Validators.required]),
            college_description: new FormControl('', [Validators.required]),
            college_logo: new FormControl('', [Validators.required]),
            college_courses_offered: new FormControl('', [Validators.required]),
            college_facebook_url: new FormControl('',[Validators.required])
        });
    }

    public resetForms() {
        this.collegeItemForm.patchValue({
            college_name: '',
            college_description: '',
            college_logo: '',
            college_courses_offered: '',
            college_facebook_url: ''
        });
    }

    public collegeItemCreateEvent() {
        if (this.isCollegeItemFormSubmitted) {
            return;
        }

        if (!this.collegeItemForm.valid) {
            Object.keys(this.collegeItemForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.collegeItemForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            return;
        }

        this.isCollegeItemFormSubmitted = true;
        const collegeItemFormData = this.collegeItemForm.value;

        this.collegesService.insertCollege(collegeItemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.collegeIDFromRoute = response.data.college_id;
                        this.router.navigate(['/colleges/' + this.collegeIDFromRoute]);
                    }

                    this.isCollegeItemFormSubmitted = false;
                },
            error => {
                    console.log('insertResolution', error);
                    this.isCollegeItemFormSubmitted = false;
            }
        );
    }

    public collegeItemUpdateEvent() {
        if (this.isCollegeItemFormSubmitted) {
            return;
        }

        if (!this.collegeItemForm.valid) {
            return;
        }

        this.isCollegeItemFormSubmitted = true;
        const collegeItemFormData = this.collegeItemForm.value;

        this.collegesService.updateCollege(this.collegeIDFromRoute, collegeItemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                if (response.success) {
                    this.collegeIDFromRoute = response.data.college_id;
                    this.router.navigate(['/colleges/' + this.collegeIDFromRoute]);
                }

                this.isCollegeItemFormSubmitted = false;
            },
            error => {
                this.isCollegeItemFormSubmitted = false;
            }
        );
    }

    public collegeItemDeleteEvent() {
        this.collegesService.deleteCollege(this.collegeIDFromRoute)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    if (response.success) {
                        this.router.navigate(['/colleges']);
                    }
                },
                error => {
                    console.log('collegeItemDeleteEvent', error);
                }
            );
    }

    public previewPage() {
        const previewURL = this.appConfig.site_url + '/colleges/' + this.collegeIDFromRoute;
        window.open(previewURL, '_blank');
    }

    get fC() {
        return this.collegeItemForm.controls;
    }

    public onCollegeDescriptionReady( editor ) {
        editor.ui.getEditableElement().parentElement.insertBefore(
            editor.ui.view.toolbar.element,
            editor.ui.getEditableElement()
        );      //
        editor.simpleUpload = {uploadUrl: 'http://sms-api.jintuitive.tech/images'};
    }

    public onMainContentChange( { editor }: ChangeEvent ) {
        // this.eventContentHolder = editor.getData();
    }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {CollegesRoutes} from './colleges.routes';
import {ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {CollegesListComponent} from './components/colleges-list.component';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {CollegesService} from './services/colleges.service';
import {CollegeItemComponent} from './components/college-item/college-item.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        CollegesRoutes,
        ReactiveFormsModule,
        CKEditorModule,
        NgxDatatableModule
    ],
    declarations: [
        CollegesListComponent,
        CollegeItemComponent
    ],
    providers: [
    ],
    exports: [
    ]
})
export class CollegesModule {}

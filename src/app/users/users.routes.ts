import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserListComponent} from './components/user-list/user-list.component';
import {UserItemComponent} from './components/user-item/user-item.component';
import {UserTypeListComponent} from './components/user-type-list/user-type-list.component';
import {UserTypeItemComponent} from './components/user-type-item/user-type-item.component';

const routes: Routes = [
    {
        path: '',
        component: UserListComponent,
        data: {
            pageTitle: 'User List Page',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: 'create',
        component: UserItemComponent,
        data: {
            pageTitle: 'Register User',
            pageRoute: 'create',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: 'user-types',
        component: UserTypeListComponent,
        data: {
            pageTitle: 'User Types List Page',
            pageRoute: 'user-types',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: 'user-types/create',
        component: UserTypeItemComponent,
        data: {
            pageTitle: 'Add User Type Page',
            pageRoute: 'user-types/create',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: 'user-types/:user_type_id',
        component: UserTypeItemComponent,
        data: {
            pageTitle: 'Update User Type Page',
            pageRoute: 'user-type-item',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: ':user_credential_id',
        component: UserItemComponent,
        data: {
            pageTitle: 'Register User',
            pageRoute: 'user-item',
            showNav: true,
            showHeader: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class UsersRoutes {}

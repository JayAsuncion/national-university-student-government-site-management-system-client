import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {forkJoin, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {UsersService} from '../../services/users.service';
import {UserTypesService} from '../../services/user-types.service';
import {CollegesService} from '../../../colleges/services/colleges.service';

@Component({
    selector: 'app-user-item',
    templateUrl: './user-item.component.html',
    styleUrls: ['../../../../assets/css_min/modules/users/user-item/user-item.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class UserItemComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();

    public pageTitle = 'Create User';
    public pageSubtitle = 'This page allows you to to manage a specific <strong>User</strong>.';
    public pageCreateButtonText = 'Register User';
    public pageDeleteButtonText = 'Delete User';

    public itemForm: FormGroup;
    public itemData = {
        email: '',
        password: '',
        user_credential_name: '',
        user_type_id: '',
        first_name: '',
        middle_name: '',
        last_name: '',
        college_id: ''
    };
    public collegeList = [];
    public userTypeList = [];

    public IDFromRoute;
    public isItemFormSubmitted = false;
    public isItemFormSubmitting = false;
    public isItemLoaded = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private collegesService: CollegesService,
        private router: Router,
        private usersService: UsersService,
        private userTypesService: UserTypesService
    ) {}

    ngOnInit(): void {
        this.IDFromRoute = this.activatedRoute.snapshot.params.user_credential_id;

        // Create Form
        if (this.IDFromRoute === undefined) {
            this.loadColleges();
            this.loadUserTypes();
            this.buildItemCreateForm();
        } else {
            this.loadColleges();
            this.loadUserTypes();
            this.pageTitle = 'Update User';
            this.loadItem(this.IDFromRoute);
            this.buildItemCreateForm();
        }
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    private buildItemCreateForm() {
        this.itemForm = new FormGroup({
            email: new FormControl('', [Validators.required]),
            user_credential_name: new FormControl('', [Validators.required]),
            password: new FormControl('', [Validators.required]),
            first_name: new FormControl('', [Validators.required]),
            middle_name: new FormControl('', [Validators.required]),
            last_name: new FormControl('', [Validators.required]),
            user_type_id: new FormControl('', [Validators.required]),
            college_id: new FormControl('', [Validators.required])
        });
    }

    private loadItem(ID) {
        this.usersService.getItem(ID)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.itemData = response.data.user;
                        this.itemForm.patchValue({
                            email: this.itemData.email,
                            user_credential_name: this.itemData.user_credential_name,
                            password: this.itemData.password,
                            user_type_id: this.itemData.user_type_id,
                            first_name: this.itemData.first_name,
                            middle_name: this.itemData.middle_name,
                            last_name: this.itemData.last_name,
                            college_id: this.itemData.college_id
                        });
                    } else {
                        this.router.navigate(['/users']);
                    }

                    this.isItemLoaded = true;
                },
                error => {
                }
        );
    }

    private loadColleges() {
        this.collegesService.getCollegeList('n')
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.collegeList = response.data.colleges;
                    }
                },
                error => {
                }
        );
    }

    private loadUserTypes() {
        this.userTypesService.getUserTypesList()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.userTypeList = response.data.user_type_list;
                        console.log(this.userTypeList);
                    }
                },
                error => {
                }
        );
    }

    public resetForms() {
        this.itemForm.patchValue({
            email: '',
            user_credential_name: '',
            password: '',
            first_name: '',
            middle_name: '',
            last_name: '',
            user_type_id: ''
        });
    }

    public itemCreateEvent() {
        this.isItemFormSubmitted = true;

        if (this.isItemFormSubmitting) {
            return;
        }

        if (!this.itemForm.valid) {
            return;
        }

        this.isItemFormSubmitting = true;

        const itemFormData = this.itemForm.value;
        console.log(itemFormData);

        this.usersService.insertItem(itemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.IDFromRoute = response.data.user_credential_id;
                        this.router.navigate(['/users/' + this.IDFromRoute]);
                    }
                    this.isItemFormSubmitted = false;
                },
                error => {
                    this.isItemFormSubmitted = false;
                }
        );
    }

    public itemUpdateEvent() {
        if (this.isItemFormSubmitted) {
            return;
        }

        if (!this.itemForm.valid) {
            return;
        }

        this.isItemFormSubmitted = true;
        const itemFormData = this.itemForm.value;

        this.usersService.updateItem(this.IDFromRoute, itemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                if (response.success) {
                    this.IDFromRoute = response.data.user_credential_id;
                    this.router.navigate(['/users/' + this.IDFromRoute]);
                }

                this.isItemFormSubmitted = false;
            },
            error => {
                this.isItemFormSubmitted = false;
            }
        );
    }

    public itemDeleteEvent() {
        this.usersService.deleteItem(this.IDFromRoute)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    this.router.navigate(['/users']);
                },
                error => {
                    console.log('itemDeleteEvent', error);
                }
            )
    }

    get fC() {
        return this.itemForm.controls;
    }
}

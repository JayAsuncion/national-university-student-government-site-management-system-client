import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {forkJoin, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {mod} from 'ngx-bootstrap/chronos/utils';
import {ModulesService} from '../../../modules/services/modules.service';
import {UserTypesService} from '../../services/user-types.service';

@Component({
    selector: 'app-user-type-item',
    templateUrl: './user-type-item.component.html',
    styleUrls: ['../../../../assets/css_min/modules/users/user-type-item/user-type-item.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class UserTypeItemComponent implements OnInit, OnDestroy {

    constructor(
        private activatedRoute: ActivatedRoute,
        private formBuilder: FormBuilder,
        private modulesService: ModulesService,
        private router: Router,
        private userTypeService: UserTypesService
    ) {}

    get fC() {
        return this.itemForm.controls;
    }
    private destroyed$ = new Subject();

    public pageTitle = 'User Type Item';
    public pageSubtitle = 'This page allows you to to manage a specific <strong>User Type Item</strong>.';
    public pageCreateButtonText = 'Create User Type';
    public pageDeleteButtonText = 'Delete User Type';

    public itemForm: FormGroup;
    public itemData = {
        user_type_label: ''
    };
    public moduleList = [];
    public checkedModuleList = [];

    public IDFromRoute;
    public isItemFormSubmitted = false;
    public isItemLoaded = false;

    static processModuleListState(moduleList, userTypeModules) {
        const moduleListLength = moduleList.length;
        const userTypeModulesLength = userTypeModules !== undefined ? userTypeModules.length : 0;

        if (userTypeModulesLength > 0) {
            for (let i = 0; i < moduleListLength; i++) {
                for (let j = 0; j < userTypeModulesLength; j++) {
                    if (moduleList[i].module_code === userTypeModules[j]) {
                        moduleList[i].checked = true;
                    }
                }
            }
        }

        return moduleList;
    }

    ngOnInit(): void {
        this.buildItemForm();

        this.IDFromRoute = this.activatedRoute.snapshot.params.user_type_id;

        // Create Form
        if (this.IDFromRoute === undefined) {
            this.loadModuleList();
        } else {
            this.loadItem(this.IDFromRoute);
        }
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    private buildItemForm() {
        this.itemForm = new FormGroup({
            user_type_label: new FormControl('', [Validators.required])
        });
    }

    private loadItem(ID) {
        const joinedResponse = forkJoin([
            this.userTypeService.getUserType(ID),
            this.modulesService.getModuleList()
        ]).pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                this.handleData(response);
            },
            error => {

            }
        );
    }

    private loadModuleList() {
        this.modulesService.getModuleList()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.moduleList = response.data.module_list;
                    }

                    this.isItemLoaded = true;
                },
                error => {
                }
        );
    }

    private handleData(data) {
        const userTypeResponse = data[0];
        const modulesResponse = data[1];

        if (userTypeResponse.success && modulesResponse.success) {
            this.itemData = userTypeResponse.data.user_type[this.IDFromRoute];
            this.itemForm.patchValue({
                user_type_label: this.itemData.user_type_label
            });
            // @ts-ignore
            if (modulesResponse.data.module_list !== undefined && modulesResponse.data.module_list.length > 0) {
                // @ts-ignore
                if (this.itemData.modules !== undefined && this.itemData.modules.length > 0) {
                    // @ts-ignore
                    this.checkedModuleList = this.itemData.modules;
                }

                this.moduleList = UserTypeItemComponent.processModuleListState(
                    modulesResponse.data.module_list, this.checkedModuleList
                );
            }
        } else {
            this.router.navigate(['/users/user-types'])
        }

        this.isItemLoaded = true;
    }

    public resetForms() {
        this.itemForm.patchValue({
            user_type_label: ''
        });
    }

    public itemCreateEvent() {
        if (this.isItemFormSubmitted) {
            return;
        }

        if (!this.itemForm.valid) {
            return;
        }

        this.isItemFormSubmitted = true;
        const itemFormData = this.itemForm.value;
        itemFormData.modules = this.checkedModuleList;

        this.userTypeService.createUserType(itemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.IDFromRoute = response.data.user_type_id;
                        this.router.navigate(['/users/user-types/' + this.IDFromRoute]);
                    }
                    this.isItemFormSubmitted = false;
                },
                error => {
                    this.isItemFormSubmitted = false;
                }
        );
    }

    public itemUpdateEvent() {
        if (this.isItemFormSubmitted) {
            return;
        }

        if (!this.itemForm.valid) {
            return;
        }

        this.isItemFormSubmitted = true;
        const itemFormData = this.itemForm.value;
        itemFormData.modules = this.checkedModuleList;

        this.userTypeService.updateUserType(this.IDFromRoute, itemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                if (response.success) {
                    this.IDFromRoute = response.data.user_type_id;
                    this.router.navigate(['/users/user-types/' + this.IDFromRoute]);
                }

                this.isItemFormSubmitted = false;
            },
            error => {
                this.isItemFormSubmitted = false;
            }
        );
    }

    public itemDeleteEvent() {
        this.userTypeService.deleteUserType(this.IDFromRoute)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    if (response.success) {
                        this.router.navigate(['/users/user-types']);
                    }
                },
                error => {
                    console.log('itemDeleteEvent', error);
                }
            );
    }

    public onModuleItemClick(event) {
        const moduleCode = event.target.value;
        const isModuleChecked = event.target.checked;

        if (isModuleChecked) {
            this.checkedModuleList.push(moduleCode);
        } else {
            const checkedModuleListCount = this.checkedModuleList.length;
            for (let i = 0; i < checkedModuleListCount; i++) {
                if (this.checkedModuleList[i] === moduleCode) {
                    this.checkedModuleList.splice(i, 1);
                }
            }
        }
    }
}

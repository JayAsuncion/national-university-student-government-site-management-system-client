import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {ColumnMode, SelectionType} from '@swimlane/ngx-datatable';
import {takeUntil} from 'rxjs/operators';
import {environment} from '../../../../environments/environment.local';
import {UsersService} from '../../services/users.service';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['../../../../assets/css_min/modules/users/user-list/user-list.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class UserListComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    private appConfig;

    ColumnMode = ColumnMode;
    SelectionType = SelectionType;

    isListLoading = true;
    pageSelected = [];
    listData: any[];

    apiListKey = 'user_list';      // Key of List in API response
    itemRoute = '/users';    // Route to redirect upon click
    itemPrimaryKey = 'user_credential_id';    // Used for redirection purposes upon click

    constructor(
        private router: Router,
        private usersService: UsersService
    ) {}

    ngOnInit(): void {
        this.appConfig = environment.app;

        this.getList();
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    private getList() {
        this.usersService.getList()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.listData = response.data[this.apiListKey];
                    }

                    this.isListLoading = false;
                },
                error => {
                }
        );
    }

    onListHover(event) {
        console.log('Activate');
    }

    onListSelect(list) {
        this.router.navigate([this.itemRoute + '/' +  list.selected[0][this.itemPrimaryKey]]);
    }
}

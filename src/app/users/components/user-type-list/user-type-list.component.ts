import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {ColumnMode, SelectionType} from '@swimlane/ngx-datatable';
import {takeUntil} from 'rxjs/operators';
import {environment} from '../../../../environments/environment.local';
import {UserTypesService} from '../../services/user-types.service';

@Component({
    selector: 'app-user-type-list',
    templateUrl: './user-type-list.component.html',
    styleUrls: ['../../../../assets/css_min/modules/users/user-type-list/user-type-list.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class UserTypeListComponent implements OnInit {
    private destroyed$ = new Subject();
    private appConfig;

    ColumnMode = ColumnMode;
    SelectionType = SelectionType;

    isListLoading = true;
    pageSelected = [];
    listData: any[];

    // Key of List in API response
    apiListKey = 'user_type_list';
    itemRoute = '/users/user-types';
    itemPrimaryKey = 'user_type_id';

    constructor(
        private router: Router,
        private userTypesService: UserTypesService
    ) {}

    ngOnInit(): void {
        this.appConfig = environment.app;

        this.getList();
    }

    private getList() {
        this.userTypesService.getUserTypesList()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.listData = response.data[this.apiListKey];
                    }

                    this.isListLoading = false;
                },
            error => {
                    this.isListLoading = false;
            }
        );
    }

    onListHover(event) {
        console.log('Activate');
    }

    onListSelect(list) {
        this.router.navigate([this.itemRoute + '/' +  list.selected[0][this.itemPrimaryKey]]);
    }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {UsersRoutes} from './users.routes';
import {ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {UserListComponent} from './components/user-list/user-list.component';
import {UserTypeListComponent} from './components/user-type-list/user-type-list.component';
import {UserTypeItemComponent} from './components/user-type-item/user-type-item.component';
import {UserItemComponent} from './components/user-item/user-item.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        UsersRoutes,
        ReactiveFormsModule,
        CKEditorModule,
        NgxDatatableModule
    ],
    declarations: [
        UserListComponent,
        UserItemComponent,
        UserTypeListComponent,
        UserTypeItemComponent
    ],
    providers: [
    ],
    exports: [
    ]
})
export class UsersModule {}

import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UsersService {
    private resourceUrl = environment.app.api_url + '/users';

    constructor(
        private httpClient: HttpClient
    ) {}

    public getList(): Observable<any> {
        return this.httpClient.get(this.resourceUrl);
    }

    public getItem(ID): Observable<any> {
        const url = this.resourceUrl + `/${ID}`;
        return this.httpClient.get(url);
    }

    public insertItem(formData): Observable<any> {
        const url = this.resourceUrl;
        return this.httpClient.post(url, formData);
    }

    public updateItem(ID, formData): Observable<any> {
        const url = this.resourceUrl + `/${ID}`;
        return this.httpClient.put(url, formData);
    }

    public deleteItem(ID): Observable<any> {
        const url = this.resourceUrl + `/${ID}`;
        const formData = {active_flag: 'n'};
        return this.httpClient.put(url, formData);
    }
}

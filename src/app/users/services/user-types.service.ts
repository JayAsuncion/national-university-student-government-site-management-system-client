import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class UserTypesService {
    private resourceUrl = environment.app.api_url + '/user-types';

    constructor(
        private httpClient: HttpClient
    ) {}

    public getUserTypesList(): Observable<any> {
        return this.httpClient.get(this.resourceUrl);
    }

    public getUserType(userTypeID): Observable<any> {
        return this.httpClient.get(this.resourceUrl + `/${userTypeID}`);
    }

    public createUserType(formData): Observable<any> {
        return this.httpClient.post(this.resourceUrl, formData);
    }

    public updateUserType(userTypeID, formData): Observable<any> {
        return this.httpClient.put(this.resourceUrl + `/${userTypeID}`, formData);
    }

    public deleteUserType(userTypeID): Observable<any> {
        const formData = {delete_flag: 'y'};
        return this.httpClient.put(this.resourceUrl + `/${userTypeID}`, formData);
    }
}

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {ResolutionsRoutes} from './resolutions.routes';
import {ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ResolutionsComponent} from './components/resolutions.component';
import {ResolutionItemComponent} from './components/resolution-item/resolution-item.component';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ResolutionsRoutes,
        ReactiveFormsModule,
        CKEditorModule,
        NgxDatatableModule
    ],
    declarations: [
        ResolutionsComponent,
        ResolutionItemComponent
    ],
    providers: [
    ],
    exports: [
    ]
})
export class ResolutionsModule {}

import {Injectable, OnInit} from '@angular/core';
import {ResolutionsModule} from '../resolutions.module';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ResolutionListService {
    private resourceUrl = environment.app.api_url;

    constructor(
        private httpClient: HttpClient
    ) {}

    public getResolutionList(): Observable<any> {
        const url = this.resourceUrl + '/resolutions';
        return this.httpClient.get(url);
    }
}

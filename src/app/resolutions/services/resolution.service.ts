import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ResolutionService {
    private resourceUrl = environment.app.api_url;

    constructor(
        private httpClient: HttpClient
    ) {}

    public getResolution(resolutionID): Observable<any> {
        const url = this.resourceUrl + '/resolutions/' + resolutionID;
        return this.httpClient.get(url);
    }

    public insertResolution(formData): Observable<any> {
        const url = this.resourceUrl + '/resolutions';
        return this.httpClient.post(url, formData);
    }

    public updateResolution(resolutionID, formData): Observable<any> {
        const url = this.resourceUrl + '/resolutions/' + resolutionID;
        return this.httpClient.put(url, formData);
    }

    public deleteResolution(resolutionID): Observable<any> {
        const formData = {delete_flag: 'y'};
        const url = this.resourceUrl + '/resolutions/' + resolutionID;
        return this.httpClient.put(url, formData);
    }
}

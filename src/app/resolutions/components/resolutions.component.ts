import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {PagesService} from '../../pages/services/pages.service';
import {Router} from '@angular/router';
import {ColumnMode, SelectionType} from '@swimlane/ngx-datatable';
import {takeUntil} from 'rxjs/operators';
import {ResolutionListService} from '../services/resolution-list.service';
import {environment} from '../../../environments/environment.local';

@Component({
    selector: 'app-contact-us',
    templateUrl: './resolutions.component.html',
    styleUrls: ['../../../assets/css_min/modules/resolutions/resolutions.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class ResolutionsComponent implements OnInit {
    private destroyed$ = new Subject();
    private appConfig;

    ColumnMode = ColumnMode;
    SelectionType = SelectionType;

    isResolutionsListLoading = true;
    pageSelected = [];
    resolutionsListData: any[];

    constructor(
        private pagesService: PagesService,
        private resolutionListService: ResolutionListService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.appConfig = environment.app;

        this.getResolutionList();
    }

    private getResolutionList() {
        this.resolutionListService.getResolutionList()
            .pipe(takeUntil(this.destroyed$)).subscribe(
            data => {
                if (data.success) {
                    this.resolutionsListData = data.data.resolution_list;
                    this.isResolutionsListLoading = false;
                }
            },
            error => {
                console.log('getResolutionList', error);
            }
        );
    }

    onResolutionListHover(event) {
        console.log('Activate');
    }

    onResolutionListSelect(resolutionList) {
        this.router.navigate(['/resolutions/' + resolutionList.selected[0].resolution_id]);
    }

    public previewPage() {
        let previewURl = this.appConfig.site_url + '/nusg-student-congress/resolutions';
        window.open(previewURl, '_blank');
    }
}

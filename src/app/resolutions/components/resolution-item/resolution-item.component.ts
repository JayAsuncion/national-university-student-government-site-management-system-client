import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {ResolutionService} from '../../services/resolution.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {environment} from '../../../../environments/environment';

@Component({
    selector: 'app-resolution-item',
    templateUrl: 'resolution-item.component.html',
    styleUrls: ['../../../../assets/css_min/modules/resolutions/resolution-item/resolution-item.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class ResolutionItemComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    private appConfig = environment.app;

    public resolutionIDFromRoute;

    public resolutionItemForm: FormGroup;
    public resolutionItemData = {
        resolution_title: '',
        resolution_description: '',
        resolution_url: ''
    };
    public isResolutionItemFormSubmitted = false;

    constructor(
        private activatedRoute: ActivatedRoute,
        private resolutionService: ResolutionService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.buildResolutionItemForm();

        this.resolutionIDFromRoute = this.activatedRoute.snapshot.params.resolution_id;

        // Create Form
        if (this.resolutionIDFromRoute === undefined) {

        } else {
            this.loadResolutionItem(this.resolutionIDFromRoute);
        }
    }

    private loadResolutionItem(resolutionID) {
        this.resolutionService.getResolution(resolutionID)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.resolutionItemData = response.data.resolution;
                        this.resolutionItemData.resolution_url = JSON.parse(this.resolutionItemData.resolution_url);
                        this.resolutionItemForm.patchValue({
                            resolution_title: this.resolutionItemData.resolution_title,
                            resolution_description: this.resolutionItemData.resolution_description,
                            resolution_url: this.resolutionItemData.resolution_url
                        });
                    } else {
                        this.router.navigate(['/resolutions']);
                    }
                },
            error => {
                console.log('loadResolutionItem', error);
            }
        );
    }

    private buildResolutionItemForm() {
        this.resolutionItemForm = new FormGroup({
            resolution_title: new FormControl('', [Validators.required]),
            resolution_description: new FormControl('', [Validators.required]),
            resolution_url: new FormControl('', [Validators.required])
        });
    }

    public resetForms() {
        this.resolutionItemForm.patchValue({
            resolution_title: '',
            resolution_description: '',
            resolution_url: ''
        });
    }

    public resolutionItemCreateEvent() {
        if (this.isResolutionItemFormSubmitted) {
            return;
        }

        if (!this.resolutionItemForm.valid) {
            return;
        }

        this.isResolutionItemFormSubmitted = true;
        const resolutionItemFormData = this.resolutionItemForm.value;

        this.resolutionService.insertResolution(resolutionItemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.resolutionIDFromRoute = response.data.resolution_id;
                        this.router.navigate(['/resolutions/' + this.resolutionIDFromRoute]);
                    }

                    this.isResolutionItemFormSubmitted = false;
                },
            error => {
                    console.log('insertResolution', error);
            }
        );
    }

    public resolutionItemUpdateEvent() {
        if (this.isResolutionItemFormSubmitted) {
            return;
        }

        if (!this.resolutionItemForm.valid) {
            return;
        }

        this.isResolutionItemFormSubmitted = true;
        const resolutionItemFormData = this.resolutionItemForm.value;

        this.resolutionService.updateResolution(this.resolutionIDFromRoute, resolutionItemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                if (response.success) {
                    this.resolutionIDFromRoute = response.data.resolution_id;
                    this.router.navigate(['/resolutions/' + this.resolutionIDFromRoute]);
                }

                this.isResolutionItemFormSubmitted = false;
            },
            error => {
                this.isResolutionItemFormSubmitted = false;
            }
        );
    }

    public resolutionItemDeleteEvent() {
        this.resolutionService.deleteResolution(this.resolutionIDFromRoute)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    if (response.success) {
                        this.router.navigate(['/resolutions']);
                    }
                },
                error => {
                    console.log('resolutionItemDeleteEvent', error);
                }
            );
    }

    get fC() {
        return this.resolutionItemForm.controls;
    }

    public previewPage() {
        let previewURL = this.appConfig.site_url + '/nusg-student-congress/resolutions/' + this.resolutionIDFromRoute;
        window.open(previewURL, '_blank');
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }
}

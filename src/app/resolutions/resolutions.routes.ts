import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ResolutionsComponent} from './components/resolutions.component';
import {ResolutionItemComponent} from './components/resolution-item/resolution-item.component';

const routes: Routes = [
    {
        path: '',
        component: ResolutionsComponent,
        data: {
            pageTitle: 'Resolutions Page',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: 'create',
        component: ResolutionItemComponent,
        data: {
            pageTitle: 'Create Resolution Page',
            pageRoute: 'create',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: ':resolution_id',
        component: ResolutionItemComponent,
        data: {
            pageTitle: 'Resolution Item Page',
            pageRoute: 'resolution-item',
            showNav: true,
            showHeader: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class ResolutionsRoutes {}

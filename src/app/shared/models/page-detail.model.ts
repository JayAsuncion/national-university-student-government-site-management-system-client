export class PageDetailModel {
    page_detail_id: number;
    page_name: string;
    page_url: string;
    page_description: string;
    college_id: number;
    page_category_code: string;
    template_code: string;
}

import {Directive, ElementRef, Input, OnDestroy, OnInit} from '@angular/core';
import {AuthService} from '../../auth/services/auth.service';
import {Subject} from 'rxjs';

@Directive({
    selector: '[appModuleCode]'
})
export class AppModuleCodeDirective implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    @Input() moduleCode;

    constructor (
        private elementRef: ElementRef,
        private authService: AuthService
    ) {
    }

    ngOnInit(): void {
        const userModules = this.authService.getUserModules();

        if (Array.isArray(this.moduleCode)) {
            let moduleCodesLength = this.moduleCode.length;

            if (userModules === undefined) {
                this.elementRef.nativeElement.style.display = 'none';
            } else {
                let isDisabled = true;

                for (let i = 0; i < moduleCodesLength; i++) {
                    if (userModules.indexOf(this.moduleCode[i]) >= 0) {
                        isDisabled = false;
                        break;
                    }
                }

                if (isDisabled) {
                    this.elementRef.nativeElement.style.display = 'none';
                }
            }
        } else {
            if (userModules === undefined || userModules.indexOf(this.moduleCode) === -1) {
                this.elementRef.nativeElement.style.display = 'none';
            }
        }
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }
}

import {Directive, EventEmitter, HostBinding, HostListener, Output} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {FileHandle} from '../interface/file-handler.interface';

@Directive({
    selector: '[appFileDragAndDrop]'
})

export class AppFileDragAndDropDirective {
    @Output() filesDropped: EventEmitter<FileHandle[]> = new EventEmitter();
    @HostBinding('style.background') private background = 'rgba(34,8,113, 0.05)';
    @HostBinding('style.color') private color = '#220871';

    constructor(
        private domSanitizer: DomSanitizer
    ) {}
3
    @HostListener('dragover', ['$event']) public onDragOver(event: DragEvent) {
        event.preventDefault();
        event.stopPropagation();
        this.background = 'rgba(34,8,113, 0.60)';
        this.color = '#FFFFFF';
    }

    @HostListener('dragleave', ['$event']) public onDragLeave(event: DragEvent) {
        event.preventDefault();
        event.stopPropagation();
        this.background = 'rgba(34,8,113, 0.05)';
        this.color = '#220871';
    }

    @HostListener('drop', ['$event']) public onDrop(event: DragEvent) {
        event.preventDefault();
        event.stopPropagation();
        this.background = 'rgba(34,8,113, 0.05)';
        this.color = '#220871';

        let files: FileHandle[] = [];

        for (let i = 0; i < event.dataTransfer.files.length; i++) {
            const file = event.dataTransfer.files[i];
            const url = this.domSanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(file));
            files.push({ file, url });
        }

        if (files.length > 0) {
            this.filesDropped.emit(files);
        }
    }
}

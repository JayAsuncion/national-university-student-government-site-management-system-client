import { NgModule } from '@angular/core';
import { AppHeaderComponent } from './components/app-header.component';
import { CommonModule } from '@angular/common';
import { NgxNavbarModule } from 'ngx-bootstrap-navbar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AppLeftNavComponent } from './components/app-left-nav.component';
import {DashboardRoutes} from '../dashboard/dashboard.routes';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';
import {CollapseModule} from 'ngx-bootstrap/collapse';
import {RouterModule} from '@angular/router';
import {AppFileDragAndDropDirective} from './directives/app-file-drag-and-drop.directive';
import {AppFileDragAndDropComponent} from './components/app-file-drag-and-drop.component';
import {ImagesModule} from '../images/images.module';
import {AppModuleCodeDirective} from './directives/module-code.directive';

@NgModule({
    imports: [
        CommonModule,
        // BrowserAnimationsModule,
        NgxNavbarModule,
        AccordionModule.forRoot(),
        CollapseModule.forRoot(),
        RouterModule,
        ImagesModule,
        BsDropdownModule.forRoot()
    ],
    declarations: [
        AppHeaderComponent,
        AppLeftNavComponent,
        AppFileDragAndDropComponent,
        AppFileDragAndDropDirective,
        AppModuleCodeDirective
    ],
    providers: [

    ],
    exports: [
        AppHeaderComponent,
        AppLeftNavComponent,
        AppFileDragAndDropComponent,
        AppFileDragAndDropDirective
    ]
})
export class SharedModule {}

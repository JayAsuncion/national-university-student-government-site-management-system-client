import {Component, Input, NgZone, OnInit} from '@angular/core';

@Component({
    selector: 'app-left-nav',
    templateUrl: 'app-left-nav.component.html'
})
export class AppLeftNavComponent implements OnInit {
    @Input() collapsed = false;

    constructor(
    ) {

    }

    ngOnInit(): void {
    }
}

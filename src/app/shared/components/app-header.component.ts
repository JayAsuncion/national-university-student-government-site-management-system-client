import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {environment} from '../../../environments/environment.local';
import {NavigationEnd, Router} from '@angular/router';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AuthService} from '../../auth/services/auth.service';
import {BsDropdownConfig} from 'ngx-bootstrap/dropdown';

@Component({
    selector: 'app-header',
    templateUrl: 'app-header.component.html',
    providers: [{ provide: BsDropdownConfig, useValue: { isAnimated: true, autoClose: true } }]
})
export class AppHeaderComponent implements OnInit, OnDestroy {
    @Input() items: Array<any>;
    private destroyed$: Subject<{}> = new Subject();
    public appConfig: any;
    public userData;
    public isUserLoggedIn: boolean;

    // Left Nav
    @Output() leftNavChangeEmitter = new EventEmitter();
    public isLeftNavCollapsed = false;

    constructor(
        private authService: AuthService,
        private router: Router
    ) {

    }

    ngOnInit(): void {
        this.userData = this.authService.getUserData();
        this.appConfig = environment.app;
        this.isUserLoggedIn = this.authService.isLoggedIn();
    }

    ngOnDestroy() {
        this.destroyed$.next();
    }

    public toggleLeftNav() {
        this.isLeftNavCollapsed = !this.isLeftNavCollapsed;
        this.leftNavChangeEmitter.emit(this.isLeftNavCollapsed);
    }

    public logout() {
        this.authService.deleteAccessToken();
        this.router.navigate(['/auth/login']);
        return;
    }
}

import {Component, forwardRef, OnDestroy, Input, OnInit, OnChanges, SimpleChanges} from '@angular/core';
import {FileHandle} from '../interface/file-handler.interface';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {ImageUploaderService} from '../../images/services/image-uploader.service';
import {takeUntil} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';


@Component({
    selector: 'app-file-drag-and-drop',
    templateUrl: 'app-file-drag-and-drop.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => AppFileDragAndDropComponent),
            multi: true
        }
    ]
})
export class AppFileDragAndDropComponent implements ControlValueAccessor, OnInit, OnChanges, OnDestroy {
    private destroyed$ = new Subject();
    @Input() initialFiles;
    @Input() folder;
    @Input() uploadType;
    @Input() maxItem;

    public disabled: boolean;
    public value: any[] = [];

    constructor(
        private imageUploaderService: ImageUploaderService
    ) { }

    ngOnInit(): void {}

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.initialFiles !== undefined && changes.initialFiles.currentValue !== null) {
            if (
                changes.initialFiles.previousValue === '' && !changes.initialFiles.firstChange ||
                !changes.initialFiles.firstChange && changes.initialFiles.currentValue.length >= 0 ||
                changes.initialFiles.firstChange && changes.initialFiles.currentValue.length > 0
            ) {
                const initialFiles = changes.initialFiles.currentValue;
                this.value = [];

                for (const file of initialFiles) {
                    if (this.uploadType === 'resolutions' || this.uploadType === 'college_courses_offered') {
                        this.value.push({file_id: file.file_id, url: file.url});
                    } else {
                        this.value.push({image_id: file.image_id, url: file.url});
                    }
                }
                this.onChanged(initialFiles);
            }
        }
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    onChanged: any = () => {};
    onTouched: any = () => {};

    writeValue(val: any): void {
        // if (Object.keys(val).length > 0 ) {
        //     this.value.push(val);s
        // }
        this.onChanged(val);
    }

    registerOnChange(fn: any): void {
        this.onChanged = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    setDisabledState(isDisabled: boolean): void {
        this.disabled = isDisabled;
    }

    filesDroppedEvent(files: FileHandle[]) {
        this.onTouched();
        this.imageUploaderService.uploadImages(files, {folder: this.folder, uploadType: this.uploadType})
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    if (this.uploadType === 'resolutions' || this.uploadType === 'college_courses_offered') {
                        const files = data.data.files;


                        for (const file of files) {
                            this.value.push({file_id: file.file_id, url: file.url});
                        }
                    } else {
                        const images = data.data.images;

                        for (const image of images) {
                            this.value.push({image_id: image.image_id, url: image.url});
                        }
                    }

                    this.onChanged(this.value);
                },
                error => {
                    console.log('filesDroppedEvent', error);
                }
            );

    }

    removeImage(index) {
        this.value.splice(index, 1);
    }
}

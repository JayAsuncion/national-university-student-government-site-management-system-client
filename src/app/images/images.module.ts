import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {ImagesRoutes} from './images.routes';
import {ImageUploaderService} from './services/image-uploader.service';

@NgModule({
    imports: [
        CommonModule,
        ImagesRoutes
    ],
    declarations: [

    ],
    providers: [
        ImageUploaderService
    ],
    exports: [
    ]
})
export class ImagesModule {}

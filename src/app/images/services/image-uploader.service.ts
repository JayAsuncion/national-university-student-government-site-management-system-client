import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of, Subject, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {FileHandle} from '../../shared/interface/file-handler.interface';
import {catchError, takeUntil} from "rxjs/operators";

@Injectable()
export class ImageUploaderService implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.image_uploader_url;

    constructor(
        private httpClient: HttpClient
    ) {}

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    uploadImages(images: FileHandle[], options: {folder: '', uploadType: ''}): Observable<any> {
        const url = this.resourceUrl + '/images';
        const requestPayload = new FormData();
        requestPayload.append('folder', options.folder);
        requestPayload.append('upload_type', options.uploadType);

        for (const image of images) {
            requestPayload.append(image.file.name, image.file, image.file.name);
        }

        return this.httpClient.post(url, requestPayload)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }
}

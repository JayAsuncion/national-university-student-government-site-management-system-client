import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {FormControl, FormGroup, ValidationErrors} from '@angular/forms';
import * as DocumentEditor from '@ckeditor/ckeditor5-build-decoupled-document';
import {environment} from '../../../environments/environment';
import {ChangeEvent} from '@ckeditor/ckeditor5-angular';
import {takeUntil} from 'rxjs/operators';
import {PagesService} from '../../pages/services/pages.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-about-us',
    templateUrl: './about-us.component.html',
    styleUrls: ['../../../assets/css_min/modules/about-us/about-us.min.css'],
})
export class AboutUsComponent implements OnInit {
    private destroyed$ = new Subject();
    private siteURL = environment.app.site_url;

    public aboutUsForm: FormGroup;
    public isAboutUsFormSubmitted = false;
    public aboutUsData = {
        vision: '',
        mission: '',
        organizational_chart: '',
        officers: ''
    };
    private pageDetailsData;
    private pageContentData = {
        vision: '',
        mission: '',
        organizational_chart: '',
        officers: ''
    };
    private formTemplateContentID;

    public visionEditor = DocumentEditor;
    public visionEditorConfig  = {
        simpleUpload: {
            uploadUrl: `${environment.app.image_uploader_url}/images?uploader=CK_EDITOR`
        }
    };
    public missionEditor = DocumentEditor;
    public missionEditorConfig  = {
        simpleUpload: {
            uploadUrl: `${environment.app.image_uploader_url}/images?uploader=CK_EDITOR`
        }
    };
    public officersEditor = DocumentEditor;
    public officersEditorConfig  = {
        simpleUpload: {
            uploadUrl: `${environment.app.image_uploader_url}/images?uploader=CK_EDITOR`
        }
    };

    constructor(
        private pagesService: PagesService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.aboutUsForm = new FormGroup({
            vision: new FormControl(''),
            mission: new FormControl(''),
            organizational_chart: new FormControl(''),
            officers: new FormControl('')
        });

        this.getAboutUsPage();
    }

    public onVisionEditorReady( editor ) {
        editor.ui.getEditableElement().parentElement.insertBefore(
            editor.ui.view.toolbar.element,
            editor.ui.getEditableElement()
        );      //
        editor.simpleUpload = {uploadUrl: 'http://sms-api.jintuitive.tech/images'};
    }

    public onMissionEditorReady( editor ) {
        editor.ui.getEditableElement().parentElement.insertBefore(
            editor.ui.view.toolbar.element,
            editor.ui.getEditableElement()
        );      //
        editor.simpleUpload = {uploadUrl: 'http://sms-api.jintuitive.tech/images'};
    }

    public onOfficersEditorReady( editor ) {
        editor.ui.getEditableElement().parentElement.insertBefore(
            editor.ui.view.toolbar.element,
            editor.ui.getEditableElement()
        );      //
        editor.simpleUpload = {uploadUrl: 'http://sms-api.jintuitive.tech/images'};
    }

    getAboutUsPage() {
        this.pagesService.fetchPage(12)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    if (!data.success) {
                        this.router.navigate(['/']);
                        return;
                    }

                    this.pageDetailsData = data.data.page_details;
                    // this.pageDetailsForm.patchValue({
                    //     page_name : this.pageDetailsData.page_name,
                    //     page_url : this.pageDetailsData.page_url,
                    //     page_description: this.pageDetailsData.page_description,
                    //     page_category_code: this.pageDetailsData.page_category_code,
                    //     template_code : this.pageDetailsData.template_code
                    // });
                    this.formTemplateContentID = data.data.page_content.form_template_content_id;
                    this.pageContentData = data.data.form_template_content;
                    this.aboutUsData = {
                        vision: this.pageContentData.vision,
                        mission: this.pageContentData.mission,
                        organizational_chart: this.pageContentData.organizational_chart,
                        officers: this.pageContentData.officers
                    };
                    this.aboutUsForm.patchValue({
                        vision: this.pageContentData.vision,
                        mission: this.pageContentData.mission,
                        organizational_chart: this.pageContentData.organizational_chart,
                        officers: this.pageContentData.officers
                    });
                },
                error => {
                    console.log(error);
                }
            );
    }

    public onMainContentChange( { editor }: ChangeEvent ) {
        // this.eventContentHolder = editor.getData();
    }

    pageEditorUpdateEvent() {
        if (this.isAboutUsFormSubmitted) {
            return;
        }

        if (this.aboutUsForm.invalid) {
            Object.keys(this.aboutUsForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.aboutUsForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            return;
        }

        this.isAboutUsFormSubmitted = true;

        const aboutUsFormData = this.aboutUsForm.value;

        if (JSON.stringify(aboutUsFormData) === JSON.stringify(this.aboutUsData)) {
            console.log('no changes');
            return;
        }

        let requestPayload = {
            page_details: this.pageDetailsData,
            page_content: {form_template_content_id: this.formTemplateContentID},
            form_template_content: aboutUsFormData
        };

        this.pagesService.updatePage(12, requestPayload).pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    this.isAboutUsFormSubmitted = false;
                },
                error => {
                    console.log('updatePage', error);
                }
            );
    }

    public previewPage() {
        let previewURL = this.siteURL + '/about-us';
        window.open(previewURL, '_blank');
    }
}

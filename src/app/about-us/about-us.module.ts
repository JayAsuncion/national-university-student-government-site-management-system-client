import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {AboutUsRoutes} from './about-us.routes';
import {AboutUsComponent} from './components/about-us.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from "@ckeditor/ckeditor5-angular";

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        AboutUsRoutes,
        ReactiveFormsModule,
        CKEditorModule
    ],
    declarations: [
        AboutUsComponent
    ],
    providers: [
    ],
    exports: [
    ]
})
export class AboutUsModule {}

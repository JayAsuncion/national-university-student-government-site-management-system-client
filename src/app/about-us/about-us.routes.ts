import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AboutUsComponent} from './components/about-us.component';

const routes: Routes = [
    {
        path: '',
        component: AboutUsComponent,
        data: {
            pageTitle: 'About Us Page',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class AboutUsRoutes {}

import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";
import {EventCategoryRoutes} from "./event-category.routes";
import {ReactiveFormsModule} from "@angular/forms";
import {EventCategoryListComponent} from "./components/event-category-list/event-category-list.component";
import {EventCategoryItemComponent} from './components/event-category-item/event-category-item.component';
import {EventCategoryService} from "./services/event-category.service";
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        EventCategoryRoutes,
        ReactiveFormsModule,
        CKEditorModule,
        NgxDatatableModule
    ],
    declarations: [
        EventCategoryListComponent,
        EventCategoryItemComponent
    ],
    providers: [
        EventCategoryService
    ],
    exports: [

    ]
})
export class EventCategoryModule {}

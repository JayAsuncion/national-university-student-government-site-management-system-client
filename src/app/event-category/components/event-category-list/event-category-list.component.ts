import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {ColumnMode, SelectionType} from '@swimlane/ngx-datatable';
import {takeUntil} from 'rxjs/operators';
import {environment} from '../../../../environments/environment.local';
import {EventCategoryService} from '../../services/event-category.service';

@Component({
    selector: 'app-event-category-list',
    templateUrl: './event-category-list.component.html',
    styleUrls: ['../../../../assets/css_min/modules/event-category/event-category-list/event-category-list.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class EventCategoryListComponent implements OnInit {
    private destroyed$ = new Subject();
    private appConfig;

    ColumnMode = ColumnMode;
    SelectionType = SelectionType;

    isListLoading = true;
    pageSelected = [];
    listData: any[];

    // Key of List in API response
    apiListKey = 'event_category_list';
    itemRoute = '/event-category';
    itemPrimaryKey = 'event_category_id';

    constructor(
        private router: Router,
        private eventCategoryService: EventCategoryService
    ) {}

    ngOnInit(): void {
        this.appConfig = environment.app;

        this.getList();
    }

    private getList() {
        this.eventCategoryService.fetchEventCategoryList()
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.listData = response.data[this.apiListKey];
                    }

                    this.isListLoading = false;
                },
            error => {
                    this.isListLoading = false;
            }
        );
    }

    onListHover(event) {
        console.log('Activate');
    }

    onListSelect(list) {
        this.router.navigate([this.itemRoute + '/' +  list.selected[0][this.itemPrimaryKey]]);
    }
}

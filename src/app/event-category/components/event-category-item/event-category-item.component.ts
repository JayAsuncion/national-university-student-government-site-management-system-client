import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {EventCategoryService} from '../../services/event-category.service';

@Component({
    selector: 'app-event-category-item',
    templateUrl: './event-category-item.component.html',
    styleUrls: ['../../../../assets/css_min/modules/event-category/event-category-item/event-category-item.min.css'],
    encapsulation: ViewEncapsulation.None
})
export class EventCategoryItemComponent implements OnInit, OnDestroy {

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventCategoryService: EventCategoryService
    ) {}

    get fC() {
        return this.itemForm.controls;
    }
    private destroyed$ = new Subject();

    public pageTitle = 'Event Category Item';
    public pageSubtitle = 'This page allows you to to manage a specific <strong>Event Category Item</strong>.';
    public pageCreateButtonText = 'Create Event Category';
    public pageDeleteButtonText = 'Delete Event Category';

    public itemForm: FormGroup;
    public itemData = {
        event_category_name: ''
    };
    public moduleList = [];
    public checkedModuleList = [];

    public IDFromRoute;
    public isItemFormSubmitted = false;
    public isItemLoaded = false;

    ngOnInit(): void {
        this.buildItemForm();

        this.IDFromRoute = this.activatedRoute.snapshot.params.event_category_id;

        // Create Form
        if (this.IDFromRoute === undefined) {

        } else {
            this.loadItem(this.IDFromRoute);
        }
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    private buildItemForm() {
        this.itemForm = new FormGroup({
            event_category_name: new FormControl('', [Validators.required])
        });
    }

    private loadItem(ID) {
        this.eventCategoryService.getItem(ID)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    if (response.success) {
                        this.itemData = response.data.event_category;
                        this.itemForm.patchValue({
                            event_category_name: this.itemData.event_category_name
                        });
                    } else {
                        this.router.navigate(['/event-category']);
                    }
                },
                error => {
                    console.log('loadItem', error);
                }
            );
    }

    public resetForms() {
        this.itemForm.patchValue({
            event_category_name: ''
        });
    }

    public itemCreateEvent() {
        if (this.isItemFormSubmitted) {
            return;
        }

        if (!this.itemForm.valid) {
            return;
        }

        this.isItemFormSubmitted = true;
        const itemFormData = this.itemForm.value;
        itemFormData.modules = this.checkedModuleList;

        this.eventCategoryService.createItem(itemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
                response => {
                    if (response.success) {
                        this.IDFromRoute = response.data.event_category_id;
                        this.router.navigate(['/event-category/' + this.IDFromRoute]);
                    }
                    this.isItemFormSubmitted = false;
                },
                error => {
                    this.isItemFormSubmitted = false;
                }
        );
    }

    public itemUpdateEvent() {
        if (this.isItemFormSubmitted) {
            return;
        }

        if (!this.itemForm.valid) {
            return;
        }

        this.isItemFormSubmitted = true;
        const itemFormData = this.itemForm.value;

        this.eventCategoryService.updateItem(this.IDFromRoute, itemFormData)
            .pipe(takeUntil(this.destroyed$)).subscribe(
            response => {
                if (response.success) {
                    this.IDFromRoute = response.data.event_category_id;
                    this.router.navigate(['/event-category/' + this.IDFromRoute]);
                }

                this.isItemFormSubmitted = false;
            },
            error => {
                this.isItemFormSubmitted = false;
            }
        );
    }

    public itemDeleteEvent() {
        this.eventCategoryService.deleteItem(this.IDFromRoute)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    if (response.success) {
                        this.router.navigate(['/event-category']);
                    }
                }, error => {
                    console.log('itemDeleteEvent', error);
                }
            )
    }
}

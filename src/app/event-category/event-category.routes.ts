import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EventCategoryListComponent} from './components/event-category-list/event-category-list.component';
import {EventCategoryItemComponent} from './components/event-category-item/event-category-item.component';

const routes: Routes = [
    {
        path: '',
        component: EventCategoryListComponent,
        data: {
            pageTitle: 'Event Category',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: 'create',
        component: EventCategoryItemComponent,
        data: {
            pageTitle: 'Event Category',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: 'create',
        component: EventCategoryItemComponent,
        data: {
            pageTitle: 'Event Category',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    },
    {
        path: ':event_category_id',
        component: EventCategoryItemComponent,
        data: {
            pageTitle: 'Event Category',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class EventCategoryRoutes {}

import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject, throwError} from 'rxjs';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {catchError, takeUntil} from 'rxjs/operators';

@Injectable()

export class EventCategoryService implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    private resourceUrl = environment.app.api_url + '/event-category';

    constructor(
        private httpClient: HttpClient
    ) {}

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
    }

    fetchEventCategoryList(): Observable<any> {
        const url = this.resourceUrl;
        return this.httpClient.get(url)
            .pipe(takeUntil(this.destroyed$), catchError((error: any) => throwError(error)));
    }

    public getItem(itemID): Observable<any> {
        return this.httpClient.get(this.resourceUrl + `/${itemID}`);
    }

    public createItem(formData): Observable<any> {
        return this.httpClient.post(this.resourceUrl, formData);
    }

    public updateItem(itemID, formData): Observable<any> {
        return this.httpClient.put(this.resourceUrl + `/${itemID}`, formData);
    }

    public deleteItem(itemID): Observable<any> {
        const formData = {delete_flag: 'y'};
        return this.httpClient.put(this.resourceUrl + `/${itemID}`, formData);
    }
}

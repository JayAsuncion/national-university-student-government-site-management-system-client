import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ContactUsComponent} from './components/contact-us.component';

const routes: Routes = [
    {
        path: '',
        component: ContactUsComponent,
        data: {
            pageTitle: 'Contact Us Page',
            pageRoute: '',
            showNav: true,
            showHeader: true
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class ContactUsRoutes {}

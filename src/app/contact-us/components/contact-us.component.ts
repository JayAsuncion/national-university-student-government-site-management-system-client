import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {FormControl, FormGroup, ValidationErrors} from '@angular/forms';
import {takeUntil} from 'rxjs/operators';
import {PagesService} from '../../pages/services/pages.service';
import {Router} from '@angular/router';
import {environment} from '../../../environments/environment';

@Component({
    selector: 'app-contact-us',
    templateUrl: './contact-us.component.html',
    styleUrls: ['../../../assets/css_min/modules/contact-us/contact-us.min.css'],
})
export class ContactUsComponent implements OnInit {
    private destroyed$ = new Subject();
    private siteURL = environment.app.site_url;

    public contactUsForm: FormGroup;
    public isContactsFormSubmitted = false;
    public contactUsData = {
        email1: '',
        email2: '',
        facebook: '',
        twitter: '',
        instagram: '',
        address1: '',
        address2: ''
    };
    private pageDetailsData;
    private pageContentData = {
        email1: '',
        email2: '',
        facebook: '',
        twitter: '',
        instagram: '',
        address1: '',
        address2: ''
    };
    private formTemplateContentID;

    constructor(
        private pagesService: PagesService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.contactUsForm = new FormGroup({
            email1: new FormControl(''),
            email2: new FormControl(''),
            facebook: new FormControl(''),
            twitter: new FormControl(''),
            instagram: new FormControl(''),
            address1: new FormControl(''),
            address2: new FormControl('')
        });

        this.getContactUsPage();
    }

    getContactUsPage() {
        this.pagesService.fetchPage(13)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(
                data => {
                    if (!data.success) {
                        this.router.navigate(['/']);
                        return;
                    }

                    this.pageDetailsData = data.data.page_details;
                    this.formTemplateContentID = data.data.page_content.form_template_content_id;
                    this.pageContentData = data.data.form_template_content;
                    this.contactUsData = {
                        email1: this.pageContentData.email1,
                        email2: this.pageContentData.email2,
                        facebook: this.pageContentData.facebook,
                        twitter: this.pageContentData.twitter,
                        instagram: this.pageContentData.instagram,
                        address1: this.pageContentData.address1,
                        address2: this.pageContentData.address2
                    };
                    this.contactUsForm.patchValue({
                        email1: this.pageContentData.email1,
                        email2: this.pageContentData.email2,
                        facebook: this.pageContentData.facebook,
                        twitter: this.pageContentData.twitter,
                        instagram: this.pageContentData.instagram,
                        address1: this.pageContentData.address1,
                        address2: this.pageContentData.address2
                    });
                },
                error => {
                    console.log(error);
                }
            );
    }


    pageEditorUpdateEvent() {
        if (this.isContactsFormSubmitted) {
            return;
        }

        if (this.contactUsForm.invalid) {
            Object.keys(this.contactUsForm.controls).forEach(key => {
                const controlErrors: ValidationErrors = this.contactUsForm.get(key).errors;

                if (controlErrors != null) {
                    Object.keys(controlErrors).forEach(keyError => {
                        console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
                    });
                }
            });
            return;
        }

        this.isContactsFormSubmitted = true;

        const contactUsFormData = this.contactUsForm.value;

        if (JSON.stringify(contactUsFormData) === JSON.stringify(this.contactUsData)) {
            return;
        }

        let requestPayload = {
            page_details: this.pageDetailsData,
            page_content: {form_template_content_id: this.formTemplateContentID},
            form_template_content: contactUsFormData
        };

        this.pagesService.updatePage(13, requestPayload).pipe(takeUntil(this.destroyed$))
            .subscribe(
                response => {
                    this.isContactsFormSubmitted = false;
                },
                error => {
                    console.log('updatePage', error);
                }
            );
    }

    public previewPage() {
        let previewURL = this.siteURL + '/contact-us';
        window.open(previewURL, '_blank');
    }
}

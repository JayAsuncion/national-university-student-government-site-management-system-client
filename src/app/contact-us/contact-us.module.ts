import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {ContactUsRoutes} from './contact-us.routes';
import {ContactUsComponent} from './components/contact-us.component';
import {ReactiveFormsModule} from '@angular/forms';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';

@NgModule({
    imports: [
        CommonModule,
        SharedModule,
        ContactUsRoutes,
        ReactiveFormsModule,
        CKEditorModule
    ],
    declarations: [
        ContactUsComponent
    ],
    providers: [
    ],
    exports: [
    ]
})
export class ContactUsModule {}

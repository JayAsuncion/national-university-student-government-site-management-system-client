export const environment = {
    production: true,
    env: 'production',
    app: {
        site_name: 'NUSG SMS',
        site_url: 'https://officialnusg.com',
        client_url:  'http://sms.officialnusg.com',
        api_url: 'http://smsapi.officialnusg.com',
        image_uploader_url: 'http://smsapi.officialnusg.com',
    },
    auth: {

    }
};

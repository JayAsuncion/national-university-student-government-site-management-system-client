export const environment = {
    production: false,
    env: 'local',
    app: {
        site_name: 'NUSG SMS',
        site_url: 'http://local.officialnusg.com',
        client_url:  'http://local-sms.officialnusg.com',
        api_url: 'http://local-sms-api.officialnusg.com',
        image_uploader_url: 'http://local-sms-api.officialnusg.com'
    },
    auth: {

    }
};

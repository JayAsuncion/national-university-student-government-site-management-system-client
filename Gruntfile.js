module.exports = function (grunt) {
    require('jit-grunt')(grunt);

    grunt.cacheMap = [];

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            target: {
                options : {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    'src/assets/css/global-styles/global-styles.css' : [
                        'src/assets/less/compiled-global-styles/global-styles.less'
                    ],
                    'src/assets/css/modules/auth/login/login.css' : [
                        'src/assets/less/modules/auth/login/login.less'
                    ],
                    'src/assets/css/modules/home/home-carousel/home-carousel.css' : [
                        'src/assets/less/modules/home/home-carousel/home-carousel.less'
                    ],
                    'src/assets/css/modules/pages/page-list/page-list.css' : [
                        'src/assets/less/modules/pages/page-list/page-list.less'
                    ],
                    'src/assets/css/modules/pages/page-details/page-details.css' : [
                        'src/assets/less/modules/pages/page-details/page-details.less'
                    ],
                    'src/assets/css/modules/about-us/about-us.css' : [
                        'src/assets/less/modules/about-us/about-us.less'
                    ],
                    'src/assets/css/modules/contact-us/contact-us.css' : [
                        'src/assets/less/modules/contact-us/contact-us.less'
                    ],
                    'src/assets/css/modules/resolutions/resolutions.css' : [
                        'src/assets/less/modules/resolutions/resolutions.less'
                    ],
                    'src/assets/css/modules/resolutions/resolution-item/resolution-item.css' : [
                        'src/assets/less/modules/resolutions/resolution-item/resolution-item.less'
                    ],
                    'src/assets/css/modules/colleges/colleges-list/colleges-list.css' : [
                        'src/assets/less/modules/colleges/colleges-list/colleges-list.less'
                    ],
                    'src/assets/css/modules/colleges/college-item/college-item.css' : [
                        'src/assets/less/modules/colleges/college-item/college-item.less'
                    ],
                    'src/assets/css/modules/users/user-list/user-list.css' : [
                        'src/assets/less/modules/users/user-list/user-list.less'
                    ],
                    'src/assets/css/modules/users/user-item/user-item.css' : [
                        'src/assets/less/modules/users/user-item/user-item.less'
                    ],
                    'src/assets/css/modules/users/user-type-list/user-type-list.css' : [
                        'src/assets/less/modules/users/user-type-list/user-type-list.less'
                    ],
                    'src/assets/css/modules/users/user-type-item/user-type-item.css' : [
                        'src/assets/less/modules/users/user-type-item/user-type-item.less'
                    ],
                    'src/assets/css/modules/event-category/event-category-list/event-category-list.css' : [
                        'src/assets/less/modules/event-category/event-category-list/event-category-list.less'
                    ],
                    'src/assets/css/modules/event-category/event-category-item/event-category-item.css' : [
                        'src/assets/less/modules/event-category/event-category-item/event-category-item.less'
                    ]
                }
            }
        },
        cssmin: {
            target: {
                files: {
                    'src/assets/css_min/global-styles/global-styles.min.css' : [
                        'src/assets/css/global-styles/global-styles.css'
                    ],
                    'src/assets/css_min/modules/auth/login/login.min.css' : [
                        'src/assets/css/modules/auth/login/login.css'
                    ],
                    'src/assets/css_min/modules/home/home-carousel/home-carousel.min.css' : [
                        'src/assets/css/modules/home/home-carousel/home-carousel.css'
                    ],
                    'src/assets/css_min/modules/pages/page-list/page-list.min.css' : [
                        'src/assets/css/modules/pages/page-list/page-list.css'
                    ],
                    'src/assets/css_min/modules/pages/page-details/page-details.min.css' : [
                        'src/assets/css/modules/pages/page-details/page-details.css'
                    ],
                    'src/assets/css_min/modules/about-us/about-us.min.css' : [
                        'src/assets/css/modules/about-us/about-us.css'
                    ],
                    'src/assets/css_min/modules/contact-us/contact-us.min.css' : [
                        'src/assets/css/modules/contact-us/contact-us.css'
                    ],
                    'src/assets/css_min/modules/resolutions/resolutions.min.css' : [
                        'src/assets/css/modules/resolutions/resolutions.css'
                    ],
                    'src/assets/css_min/modules/resolutions/resolution-item/resolution-item.min.css' : [
                        'src/assets/css/modules/resolutions/resolution-item/resolution-item.css'
                    ],
                    'src/assets/css_min/modules/colleges/colleges-list/colleges-list.min.css' : [
                        'src/assets/css/modules/colleges/colleges-list/colleges-list.css'
                    ],
                    'src/assets/css_min/modules/colleges/college-item/college-item.min.css' : [
                        'src/assets/css/modules/colleges/college-item/college-item.css'
                    ],
                    'src/assets/css_min/modules/users/user-list/user-list.min.css' : [
                        'src/assets/css/modules/users/user-list/user-list.css'
                    ],
                    'src/assets/css_min/modules/users/user-item/user-item.min.css' : [
                        'src/assets/css/modules/users/user-item/user-item.css'
                    ],
                    'src/assets/css_min/modules/users/user-type-list/user-type-list.min.css' : [
                        'src/assets/css/modules/users/user-type-list/user-type-list.css'
                    ],
                    'src/assets/css_min/modules/users/user-type-item/user-type-item.min.css' : [
                        'src/assets/css/modules/users/user-type-item/user-type-item.css'
                    ],
                    'src/assets/css_min/modules/event-category/event-category-list/event-category-list.min.css' : [
                        'src/assets/css/modules/event-category/event-category-list/event-category-list.css'
                    ],
                    'src/assets/css_min/modules/event-category/event-category-item/event-category-item.min.css' : [
                        'src/assets/css/modules/event-category/event-category-item/event-category-item.css'
                    ]
                }
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
            },
            libraries: {
                files: {

                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('default',['less', 'uglify', 'cssmin']);
};
